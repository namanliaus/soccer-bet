
     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--,
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    -----------------------------------------------------------------


Welcome to your Node.js project on Cloud9 IDE!

## Running the server

1) Open a Terminal and run mongod to start MongoDB

2) Open `app.js` and start the app by clicking on the "Run" button in the top menu.

3) Alternatively you can launch the app from the Terminal:

    $ node server.js

Once the server is running, open the project in the shape of 'https://soccerbet-sgvny2k.c9.io/'.

## Create admin user at the first time

The application does not come with database so the first task to do is to create an administrator for the app.

1) Modify createAdminUser function in web/models/dev.js for admin details

2) Make sure `module.exports = createAdminUser`

3) run

    $ node test.js

and the user will be created.

nam.anliaus@gmail.com / admin123 is being used.

## Market data

An admin can add a match and its market data.

1) Data are from `https://www.williamhill.com.au/`. With Google Chrome in inspection mode, we should be able to extract the markets from each of the tabs using:

    $("div[data-sport-section-id]")

and copy the results in the console.

Since odds are selected in `sections` with `h3.is-open`, admin user needs to open/close the markets accordingly before doing the above selection.

2) The second parsing round is with out application. The parsing code is saved in `web/views/admin/parsing_code.js`. The code parse the content of textarea#htmlCode and push the market data to `markets` array.

3) Admin can click the market header to remove it while reviewing before submitting the markets to the server.