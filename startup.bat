@echo off
start "mongoDB" /D %~dp0..\mongodb\bin mongod.exe --journal --dbpath ..\data

set curdir="%~dp0"
cd ..\..\..\nodejs
call nodevars.bat
cd /d %curdir%

REM set NODE_ENV=development
set NODE_ENV=production
echo Running in %NODE_ENV% mode

nodemon app.js