// Express and its modules create our web server
var express           =    require('express');
var errorhandler      =    require('errorhandler');
var favicon           =    require('serve-favicon');
var logger            =    require('morgan');
var bodyParser        =    require('body-parser');
var methodOverride    =    require('method-override');
var session           =    require('express-session');
var cookieParser      =    require('cookie-parser');
var accepts           =    require('accepts');

var config            =    require('./config');
var alert             =    require('./web/libs/alert_json');

var http              =    require('http');
var path              =    require('path');

var app               =    express();

var env = process.env.NODE_ENV || 'development';
if ('development' === env) {
  app.use(errorhandler());
  app.locals.pretty = true;
};

// all environments
app.set('views', path.join(__dirname, 'web/views', config.view));
app.set('view engine', 'jade');
app.use(favicon('./web/public/Soccer_ball_32.png'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(methodOverride());
app.use(cookieParser());
app.use(session({secret: 'khongcogilakhongcogi', resave: false, saveUninitialized: false}));
app.use(express.static(path.join(__dirname, 'web/public')));

function NotFound(msg, extra) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = msg;
  this.extra = extra;
}

function handleErrors(err, req, res, next) {
  if (err instanceof NotFound) {
    var accept = accepts(req);
    res.status(404);
    switch(accept.type(['json', 'html'])) {
      case 'json':
        res.send(alert.message(alert.CONST.ERRO_NO_RESOURCE));
        break;
      case 'html':
        res.render('../error/404.jade', { title: "404 " + alert.CONST.ERRO_NO_RESOURCE });
        break;
      default:
        res.setHeader('Content-Type', 'text/plain');
        res.write("404 " + alert.CONST.ERRO_NO_RESOURCE);
        res.end();
        break;
    }
  } else {
    if ('development' === env) {
      console.log(err.stack);
    }
    var accept = accepts(req);
    res.status(500);
    switch(accept.type(['json', 'html'])) {
      case 'json':
        res.send(alert.message(alert.CONST.ERRO_SERVER_PROBLEM));
        break;
      case 'html':
        res.render('../error/500.jade', { title: "500 " + alert.CONST.ERRO_SERVER_PROBLEM });
        break;
      default:
        res.setHeader('Content-Type', 'text/plain');
        res.write("500 " + alert.CONST.ERRO_SERVER_PROBLEM);
        res.end();
        break;
    }
  }
}

var index             =    require('./web/routes/index').execute;
var login             =    require('./web/routes/login').execute;
var logout            =    require('./web/routes/logout').execute;
var requestmanager    =    require('./web/routes/requestmanager');

app.get('/', index);
app.post('/login', login);
app.post('/logout', logout);
app.get(/\/(admin|op|player|public)\/.+\.js/, requestmanager.redirect);
app.post(/\/(admin|op|player)\/.+/, requestmanager.redirect);

// all other URLs are invalid
app.get('*', function(req, res, next) {
  throw new NotFound();
});
app.post('*', function(req, res, next) {
  throw new NotFound();
});

// connect to database
require('./mongoose');

// start the http server
http.createServer(app).listen(process.env.PORT, process.env.IP, function() {
  console.log('Express server listening on ' + process.env.IP + ":" + process.env.PORT);
});

// this should be down here for successfully handling errors
app.use(handleErrors);