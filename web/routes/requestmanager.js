var alert = require('../libs/alert_json');
var config = require('../../config');
var path = require('path');

module.exports.redirect = function (req, res) {
  var pathName = req._parsedUrl.pathname;
  
  // general data are available publicly
  // admin, operator, or player specific data requires logging in
  var needPrivilege = false;
  if (pathName.substr(0, 7) === '/admin/' || pathName.substr(0, 4) === '/op/' || pathName.substr(0, 8) === '/player/') {
    needPrivilege = true;
  }
  
  if (req.session.userinfo) {
    console.log('=====', req.session.userinfo.userName, '[', new Date(), '] =====');
  } else if (needPrivilege) {
    // return an error message if user has not logged in
    res.json(alert.message(alert.CONST.ERRO_NO_LOGIN_INFO));
    return;
  }
  
  // one user cannot access higher role resources, terminate serving here
  if (pathName.substr(0, 7) === '/admin/' && req.session.userinfo.userName !== 'Admin') {
    res.json(alert.message(alert.CONST.ERRO_NO_PERMISSION));
    return;
  } else if (pathName.substr(0, 4) === '/op/' && req.session.userinfo.usertype > 1) {
    res.json(alert.message(alert.CONST.ERRO_NO_PERMISSION));
    return;
  }

  // dynamic components requested at run-time
  var suffix = '.js';
  var dirName = '';
  var actualFile = pathName;
  
  var env = process.env.NODE_ENV || 'development';
  // while developing, redirect to JSX files directly
  if ('development' === env) {
    dirName = '/JSX/';
    actualFile += 'x';
  }
  
  if (pathName.indexOf(suffix, pathName.length - suffix.length) !== -1) {
    res.sendFile(actualFile, {root: path.resolve('web/views/' + config.view + dirName)});
    return;
  }
  
  // execute required resources.
  // Resource missing is already handled at top level.
  var action = require('.' + pathName);
  action.execute(req, res);
}