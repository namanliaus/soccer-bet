var userModel = require("../../models/user");

module.exports.execute = function (req, res) {
  userModel.getUserRemainingPoint({userId: req.session.userinfo.id, json: true}, function(err, msg) {
    res.json(msg);
  });
}