var matchModel = require("../../models/match");

module.exports.execute = function (req, res) {
  matchModel.getUserSlips({
    userId: req.session.userinfo.id,
    timeFilter: {past: false, longFuture: true}
  }, function(err, msg) {
    res.json(msg);
  });
}