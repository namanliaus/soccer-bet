var alert = require('../../libs/alert_json');

module.exports.execute = function (req, res) {
  // format the page to show
  var requiredFiles = ['player/sponsor_list.js'];
  if (req.session.userinfo.userType !== 1) {
    requiredFiles.unshift('player/sponsor_button.js');
  }
  res.json(alert.message(alert.CONST.INFO_JSON_RETURNED, {requiredFiles: requiredFiles}));
}