var userModel = require("../../models/user");

module.exports.execute = function (req, res) {
  userModel.getRankingList(function(err, msg) {
    res.json(msg);
  });
}