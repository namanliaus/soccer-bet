var matchModel = require("../../models/match");

module.exports.execute = function (req, res) {
  matchModel.manipulateSlip({
    _slip: JSON.parse(req.body.slip),
    userId: req.session.userinfo.id
  }, function(err, msg) {
    res.json(msg);
  });
}
