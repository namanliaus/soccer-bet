var matchModel = require("../../models/match");

module.exports.execute = function (req, res) {
  matchModel.getSlipHistory({
    // apply more time restriction
    timeFilter: {filter: req.body.filter, future: false, past: true}
  }, function(err, msg) {
    res.json(msg);
  });
}