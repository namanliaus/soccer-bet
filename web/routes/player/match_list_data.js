var matchModel = require("../../models/match");

module.exports.execute = function (req, res) {
  matchModel.getMatchesByTime({
    // apply more time restriction
    timeFilter: {filter: req.body.filter, future: true, past: false}
  }, function(err, msg) {
    res.json(msg);
  });
}