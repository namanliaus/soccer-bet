var alert = require('../../libs/alert_json');

module.exports.execute = function (req, res) {
  // format the page to show
  var requiredFiles = ['player/change_password.js', 'player/settings.js'];
  res.json(alert.message(alert.CONST.INFO_JSON_RETURNED, {requiredFiles: requiredFiles}));
}