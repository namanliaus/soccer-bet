var alert = require('../../libs/alert_json');

module.exports.execute = function (req, res) {
  // format the page to show
  var requiredFiles = ['player/time_filter_panel.js' , 'player/odd_item.js', 'player/market_item.js', 'player/match_item.js', 'player/match_list.js', 'player/match_list_boot.js'];
  res.json(alert.message(alert.CONST.INFO_JSON_RETURNED, {requiredFiles: requiredFiles}));
}