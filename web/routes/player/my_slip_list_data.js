var matchModel = require("../../models/match");

module.exports.execute = function (req, res) {
  
  matchModel.getMatchesWithSlips({
    userId: req.session.userinfo.id, 
    timeFilter: {filter: req.body.filter, future: true, past: true}
  }, function(err, msg) {
    res.json(msg);
  });
}