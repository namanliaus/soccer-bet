var userModel = require("../../models/user");

module.exports.execute = function (req, res) {
  userModel.changeUserPassword({
    userId: req.session.userinfo.id,
    oldPassword: req.body.oldPassword,
    newPassword: req.body.newPassword
  }, function(err, msg) {
    res.json(msg);
  });
}