var menuModel = require('../../models/menu');

module.exports.execute = function(req, res) {
  menuModel.formatMenuData(req.session.userinfo, function(menu) {
    res.json(menu);
  });
}