var userModel = require("../../models/user");

module.exports.execute = function (req, res) {
  userModel.sponsor({userId: req.session.userinfo.id}, function(err, msg) {
    res.json(msg);
  });
}