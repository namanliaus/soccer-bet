var matchModel = require("../../models/match");

module.exports.execute = function (req, res) {
  matchModel.getSlipHistory({
    timeFilter: {req.body.filter, future: false}
  }, function(err, msg) {
    res.json(msg);
  });
}