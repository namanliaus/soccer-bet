var matchModel = require('../../models/match');

module.exports.execute = function (req, res) {

  matchModel.saveMarkets({
    markets: JSON.parse(req.body.markets),
    matchId: req.body.matchId,
    timeFilter: {filter: req.body.filter, future: true, past: true},
  }, function(err, msg) {
    res.json(msg);
  });
}