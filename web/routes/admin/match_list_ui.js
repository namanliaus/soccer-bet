var alert = require('../../libs/alert_json');

module.exports.execute = function (req, res) {
  // format the page to show
  var requiredFiles = ['admin/match_edit_buttons.js', 'player/animation_item.js', 'admin/market_dialog.js', 'admin/match_edit_dialog.js', 'player/time_filter_panel.js', 'op/match_item.js', 'op/match_list.js'];
  
  res.json(alert.message(alert.CONST.INFO_JSON_RETURNED, {requiredFiles: requiredFiles}));
}