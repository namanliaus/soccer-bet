var userModel = require('../../models/user');

module.exports.execute = function (req, res) {
  userModel.saveUser({user: req.body}, function(err, msg) {
    res.json(msg);
  });
}