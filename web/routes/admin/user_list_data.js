var userModel = require('../../models/user');

module.exports.execute = function (req, res) {
  userModel.getUserList(function(err, msg) {
    res.json(msg);
  });
}