var alert = require('../../libs/alert_json');

module.exports.execute = function (req, res) {
  // format the page to show
  res.json(alert.message(alert.CONST.INFO_JSON_RETURNED, {requiredFiles: ['admin/user_edit_dialog.js', 'admin/user_item.js', 'admin/user_list.js']}));
}