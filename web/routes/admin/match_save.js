var matchModel = require('../../models/match');

module.exports.execute = function (req, res) {
  matchModel.saveMatch({
    match: JSON.parse(req.body.match),
    timeFilter: {filter: req.body.filter, future: true, past: true}
  }, function(err, msg) {
    res.json(msg);
  });
}