var userModel = require('../../models/user');

module.exports.execute = function (req, res) {
  userModel.deleteUser({userId: req.body.id}, function(err, msg) {
    res.json(msg);
  });
}