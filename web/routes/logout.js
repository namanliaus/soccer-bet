var alert = require('../libs/alert_json');

module.exports.execute = function(req, res) {
  // application tries to check FB login first. When it finds no FB login presented,
  // it calls logout with facebook param to clear previous login session data.
  // However, if previous login was not with FB then we should keep the session.
  if (req.body.FB) {
    if (req.session.userinfo && !req.session.userinfo.fbId) {
      // do not clear session
      var data = null
      if (req.session.userinfo.userType === 1) {
        data = {isAdmin: true};
      }
      res.send(alert.message(alert.CONST.INFO_LOGGED_IN, data));
      return;
    }
  }
  
  if (req.session.userinfo !== undefined) {
    // clear session data
    delete req.session.userinfo;
  }
  // inform client
  res.send(alert.message(alert.CONST.INFO_LOGGED_OUT));
}