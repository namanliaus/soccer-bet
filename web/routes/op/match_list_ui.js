var alert = require('../../libs/alert_json');

module.exports.execute = function (req, res) {
  // format the page to show
  var requiredFiles = ['player/time_filter_panel.js' , 'op/match_item.js', 'op/match_list.js'];
  
  res.json(alert.message(alert.CONST.INFO_JSON_RETURNED, {requiredFiles: requiredFiles}));
}