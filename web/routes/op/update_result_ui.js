var alert = require('../../libs/alert_json');

module.exports.execute = function (req, res) {
  // format the page to show
  // reuse 2 components match_item and market_item from player
  var requiredFiles = ['op/update_odd_item.js', 'player/market_item.js', 'player/match_item.js', 'op/update_result.js'];
  
  res.json(alert.message(alert.CONST.INFO_JSON_RETURNED, {requiredFiles: requiredFiles, vars: {updatingMatchId: req.body.matchId}}));
}