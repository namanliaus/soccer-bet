var matchModel = require("../../models/match");

module.exports.execute = function (req, res) {
  matchModel.getOneMatchForUpdatingResult({
    matchId: req.body.matchId
  }, function(err, msg) {
    res.json(msg);
  })
}