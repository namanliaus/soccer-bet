var matchModel = require("../../models/match");

module.exports.execute = function (req, res) {
  
  matchModel.updateMatchResult({
    matchId: req.body.matchId,
    result: req.body.result,
    wonOdds: req.body.wonOdds
  }, function(err, msg) {
    res.json(msg);
  });
}