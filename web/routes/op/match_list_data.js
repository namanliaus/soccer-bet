var matchModel = require("../../models/match");

module.exports.execute = function (req, res) {
  matchModel.getOpMatchList({
    timeFilter: {filter: req.body.filter, future: (req.session.userinfo.userType == 1) ? true : false, past: true}
  }, function(err, msg) {
    res.json(msg);
  })
}