module.exports.execute = function(req, res) {
  var env = process.env.NODE_ENV || 'development';
  var js = "javascript";
  if ('development' === env) {
    js = "jsx";
  }
  res.render("../index.jade", {js: js});
}