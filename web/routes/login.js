var userModel = require('../models/user');
var alert = require('../libs/alert_json');

module.exports.execute = function(req, res) {

  if (req.session.userinfo !== undefined) {
    // if already logged in, keep the session
    var data = {userId: req.session.userinfo.id}
    if (req.session.userinfo.userType === 1) {
      data.isAdmin = true;
    }
    res.json(alert.message(alert.CONST.INFO_LOGGED_IN, data));
    return;
  }

  // get parameters from request
  var username = req.body.name || null;
  var email = req.body.email || null;
  var fbId = req.body.id || null;
  var password = req.body.password || null;
  
  // ID (email or FacebookID) is required
  if (email === null && fbId === null) {
    res.json(alert.message(alert.CONST.ERRO_ID_REQUIED));
    return;
  }
  
  // userid or password is required
  if (fbId === null && password === null) {
    res.json(alert.message(alert.CONST.ERRO_PWD_REQUIRED));
    return;
  }
  
  userModel.userLogin({
    fbId: fbId,
    email: email,
    username: username,
    password: password
  }, function(err, msg) {
    if (err) {
      // any problem happens; return from here
      res.json(msg);
      return;
    }
    
    // successfully log in
    // save user data to session
    req.session.userinfo = {
      id: msg._id,
      fbId: fbId,
      userName: msg.userName,
      //password: msg.password,
      email: msg.email,
      initPoint: msg.initPoint,
      userPoint: msg.userPoint,
      userType: msg.userType
    };
 
    var data = {
      userId: msg._id
    };
    if (msg.userType === 1) {
      data.isAdmin = true;
    }
    res.json(alert.message(alert.CONST.INFO_LOGGED_IN, data));
  });
}