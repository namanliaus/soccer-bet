var UserEditDialog = React.createClass({displayName: "UserEditDialog",
  
  handleChanges: function(event) {
    this.props.onEdit(event);
  },
  
  handleSaveClick: function(e) {
    this.props.action();
  },
  
  componentDidMount: function() {
    $(this.getDOMNode())
    // the modal dialog may be closed in many ways
    // handle this event for cleanning data
    .on("hidden.bs.modal", function(e) {
      // inform parent that it is hiding
      this.props.onDialogHidden();
    }.bind(this))
    // show the dialog
    .modal({
      keyboard: true,
      show: true
    });
  },
  
  componentWillUnmount: function() {
    $(this.getDOMNode()).modal("hide");
  },
  
  render: function() {
    var options = this.props.options.map(function(opt, idx) {
      if (idx > 0) {
        return (React.createElement("option", {value: idx, key: idx}, opt));
      }
    });
    
    return (
      React.createElement("div", {className: "modal", id: "user-modal", tabIndex: "-1", role: "dialog", "aria-labelledby": "modal-label"}, 
        React.createElement("div", {className: "modal-dialog", role: "document"}, 
          React.createElement("div", {className: "modal-content"}, 
            React.createElement("div", {className: "modal-header"}, 
              React.createElement("button", {type: "button", className: "close", "data-dismiss": "modal", "aria-label": "Close"}, React.createElement("span", {"aria-hidden": "true"}, "×")), 
              React.createElement("h4", {className: "modal-title", id: "modal-label"}, this.props.title)
            ), 
            React.createElement("div", {className: "modal-body"}, 
              React.createElement("div", {className: "well well-user"}, 
                React.createElement("form", {id: "user-form", className: "form-horizontal"}, 
                  React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {htmlFor: "fbId", className: "col-sm-3 control-label"}, "Facebook ID"), 
                    React.createElement("div", {className: "col-sm-9"}, 
                      React.createElement("input", {type: "text", id: "fbId", name: "fbId", className: "form-control", placeholder: "Facebook ID", value: this.props.user.fbId, onChange: this.handleChanges})
                    )
                  ), 
                  React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {htmlFor: "email", className: "col-sm-3 control-label"}, "Email"), 
                    React.createElement("div", {className: "col-sm-9"}, 
                      React.createElement("input", {type: "email", id: "email", name: "email", className: "form-control", placeholder: "Email", value: this.props.user.email, onChange: this.handleChanges})
                    )
                  ), 
                   React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {htmlFor: "password", className: "col-sm-3 control-label"}, "Password"), 
                    React.createElement("div", {className: "col-sm-9"}, 
                      React.createElement("input", {type: "password", id: "password", name: "password", className: "form-control", placeholder: "Password", value: this.props.user.password, onChange: this.handleChanges})
                    )
                  ), 
                  React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {htmlFor: "username", className: "col-sm-3 control-label"}, "User Name"), 
                    React.createElement("div", {className: "col-sm-9"}, 
                      React.createElement("input", {type: "text", id: "userName", name: "username", className: "form-control", placeholder: "User Name", value: this.props.user.userName, onChange: this.handleChanges})
                    )
                  ), 
                  React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {htmlFor: "initpoint", className: "col-sm-3 control-label"}, "Init Point"), 
                    React.createElement("div", {className: "col-sm-9"}, 
                      React.createElement("input", {type: "text", id: "initPoint", name: "initpoint", className: "form-control", placeholder: "Init Point", value: this.props.user.initPoint, onChange: this.handleChanges})
                    )
                  ), 
                  React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {htmlFor: "usertype", className: "col-sm-3 control-label"}, "User Type"), 
                    React.createElement("div", {className: "col-sm-9"}, 
                      React.createElement("select", {className: "form-control", id: "userType", name: "usertype", value: (this.props.user.userType)?this.props.user.userType:3, onChange: this.handleChanges}, 
                        options
                      )
                    )
                  )
                )
              )
            ), 
            React.createElement("div", {className: "modal-footer"}, 
              React.createElement("button", {type: "button", className: "btn btn-default", "data-dismiss": "modal"}, "Close"), 
              React.createElement("button", {type: "button", className: "btn btn-primary", id: "btn-save-user", onClick: this.handleSaveClick}, "Save changes")
            )
          )
        )
      )
    );
  }
});
