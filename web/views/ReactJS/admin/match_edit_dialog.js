var MatchEditDialog = React.createClass({displayName: "MatchEditDialog",
  handleChange: function(event) {
    var match = this.state.match;
    match[event.target.id] = event.target.value;
    this.setState({match: match});
  },

  getInitialState: function() {
    return {
      title: "Add new match",
      match: {},
      action: function() {}
    }
  },

  componentDidMount: function() {
    $(React.findDOMNode(this)).find("button#btn-save-match").bind("click", function() {
      this.state.action(this.state.match);
    }.bind(this));

    $("#datetimepicker").datetimepicker({
      format: DATETIME_FORMAT,
      locale: "en",
    });
  },

  showDialog: function(data) {
    delete this.state.error;
    this.setState(data);
    // a valid way to set date: via widget function
    var mm = null;
    if (!data.match.time) {
      mm = moment(new Date());
    } else {
      mm = moment.unix(data.match.time).local();
    }
    $("#datetimepicker").data("DateTimePicker").date(mm);

    $(React.findDOMNode(this)).modal({
      keyboard: true,
      show: true
    });
  },

  hideDialog: function() {
    $(React.findDOMNode(this)).modal("hide");
  },

  saveMatch: function() {
    var mm = $("#datetimepicker").data("DateTimePicker").date();
    if (mm == null) {
      this.setState({error: "Invalid date time format. Please check."});
      return;
    } else {
      var _match = this.state.match;
      // save time in UTC
      _match.time = mm.utc().unix();
      this.setState({match: _match});
    }
    requestJSON({
      url: "admin/match_save",
      data: {
        match: JSON.stringify(this.state.match),
        filter: JSON.stringify(this.props.parent.state.filter)
      },
      dataType: "json",
      method: "POST",
      success: function(json) {
        if (json.status == "ok") {
          this.props.parent.setState(json.data);
          this.hideDialog();
        } else {
          this.setState({error: "Error " + json.code + ". " + json.message});
        }
      }.bind(this)
    });
  },

  render: function() {
    var alertMsg = null;
    if (this.state.error) {
      alertMsg = React.createElement("div", {className: "alert alert-danger alert-xs"}, this.state.error);
    }
    return (
      React.createElement("div", {className: "modal", id: "match-modal", tabIndex: "-1", role: "dialog", "aria-labelledby": "modal-label"}, 
        React.createElement("div", {className: "modal-dialog", role: "document"}, 
          React.createElement("div", {className: "modal-content"}, 
            React.createElement("div", {className: "modal-header"}, 
              React.createElement("button", {type: "button", className: "close", "data-dismiss": "modal", "aria-label": "Close"}, React.createElement("span", {"aria-hidden": "true"}, "×")), 
              React.createElement("h4", {className: "modal-title", id: "modal-label"}, this.state.title)
            ), 
            React.createElement("div", {className: "modal-body"}, 
              alertMsg, 
              React.createElement("div", {className: "well well-match"}, 
                React.createElement("form", {id: "match-form", className: "form-horizontal"}, 
                  React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {htmlFor: "name", className: "col-sm-2 control-label"}, "Match of"), 
                    React.createElement("div", {className: "col-sm-10"}, 
                      React.createElement("input", {type: "text", id: "name", name: "name", className: "form-control", placeholder: "Match", value: this.state.match.name, onChange: this.handleChange})
                    )
                  ), 
                  React.createElement("div", {className: "form-group"}, 
                    React.createElement("label", {htmlFor: "match-datetime", className: "col-sm-2 control-label"}, "Date/Time"), 
                    React.createElement("div", {className: "col-sm-10"}, 
                      React.createElement("input", {id: "time", type: "text", placeholder: DATETIME_FORMAT, className: "form-control", id: "datetimepicker"})
                    )
                  )
                )
              )
            ), 
            React.createElement("div", {className: "modal-footer"}, 
              React.createElement("button", {type: "button", className: "btn btn-default", "data-dismiss": "modal"}, "Close"), 
              React.createElement("button", {type: "button", className: "btn btn-primary", id: "btn-save-match"}, "Save changes")
            )
          )
        )
      )
    );
  }
});

// Components must be rendered (created) within its owner's render function
// we cannot create them here and reference to them in the render function later.
// Solution: create components in a render function of objects and set the objects
// to the state of pageComponent

// wait for match list ready then set new state for it to show admin components
function waitForMatchListReady() {
  var matchList = pageComponent.refs.matchList;

  if (!matchList || matchList.state.buttonAdd) {
    setTimeout(waitForMatchListReady, 100);
    return;
  }

  matchList.setState({
    buttonAdd: {
      render: function() {
        return React.createElement(AddButton, {parent: matchList});
      }
    },
    buttons: {
      render: function(matchItem) {
        return React.createElement(ControlButtons, {parent: matchList, matchItem: matchItem});
      }
    },
    marketDialog: {
      render: function() {
        return React.createElement(MarketDialog, {parent: matchList, ref: "marketDialog"});
      }
    },
    matchDialog: {
      render: function() {
        return React.createElement(MatchEditDialog, {parent: matchList, ref: "matchDialog"});
      }
    }
  });
}

waitForMatchListReady();