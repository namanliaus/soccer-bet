var AddButton = React.createClass({displayName: "AddButton",
  componentDidMount: function() {
    $(React.findDOMNode(this)).bind("click", function() {
      var dialog = this.refs.matchDialog;
      dialog.showDialog({
        title: "Add a new match",
        match: {},
        action: dialog.saveMatch
      });
    }.bind(this.props.parent));
  },

  render: function() {
    return (
      React.createElement("button", {className: "btn btn-success btn-sm", title: "Add Match"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus-sign"})
      )
    );
  }
});

var ControlButtons = React.createClass({displayName: "ControlButtons",
  editMatch: function() {
    var dialog = this.props.parent.refs.matchDialog;
    dialog.showDialog({
      title: "Edit match",
      match: this.props.matchItem.props.match,
      action: dialog.saveMatch
    });
  },

  addMarkets: function() {
    var dialog = this.props.parent.refs.marketDialog;
    dialog.showDialog(this.props.matchItem.props.match._id);
  },

  render: function() {
    return (
      React.createElement("div", {style: {display: "inline"}}, 
        React.createElement("button", {type: "button", className: "btn btn-default btn-sm", id: "edit-match", title: "Edit", onClick: this.editMatch}, 
          React.createElement("span", {className: "glyphicon glyphicon-edit", "aria-hidden": "true"})
        ), " ", 
        React.createElement("button", {type: "button", className: "btn btn-default btn-sm", id: "edit-market", title: "Add market (" + this.props.matchItem.props.match.marketCount + ")", onClick: this.addMarkets}, 
          React.createElement("span", {className: "glyphicon glyphicon-list-alt", "aria-hidden": "true"})
        ), " "
      )
    )
  }
});
