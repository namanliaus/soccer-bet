var ReactTransitionGroup = React.addons.TransitionGroup;

var OddItem = React.createClass({displayName: "OddItem",
  render: function() {
    return (
      React.createElement("div", {className: "col-xs-" + this.props.gridSize + " odd-cell"}, 
        React.createElement("div", {className: "odd"}, 
          React.createElement("div", {className: "odd-name"}, this.props.data.name), 
          React.createElement("div", {className: "odd-rate"}, this.props.data.rate)
        )
      )
    );
  }
});

var MarketItem = React.createClass({displayName: "MarketItem",

  handleClick: function() {
    // pass the event up to owners
    this.props.onRemoveItem();
  },

  render: function() {
    var colCount = 0;
    if (this.props.market.odds.length % 3 == 0 || this.props.market.odds.length > 4) {
      colCount = 3;
    } else {
      colCount = 2;
    }
    var gridSize = 12 / colCount;

    // render odd items
    var odds = this.props.market.odds.map(function(odd, idx) {
      return React.createElement(OddItem, {gridSize: gridSize, data: odd, key: idx});
    });

    // render the market;
    return (
      React.createElement("div", {className: "market-item"}, 
        React.createElement("div", {className: "market-head", onClick: this.handleClick}, this.props.market.name), 
        React.createElement("div", {className: "row row-odds"}, 
          odds
        )
      )
    );
  }
});

var Markets = React.createClass({displayName: "Markets",
  handleRemoveItem: function(idx) {
    // pass the event up to owner
    this.props.onRemoveItem(idx);
  },

  render: function() {
    var markets = [];
    if (this.props.markets) {
      markets = this.props.markets.map(function (market, idx) {
        return (
          React.createElement(AnimationItem, {transitionName: "listitem-hide", key: "listitem-hide-"+market.name, numTransIn: "1", numTransOut: "2"}, 
            React.createElement(MarketItem, {market: market, key: market.name, onRemoveItem: this.handleRemoveItem.bind(null, idx)})
          )
        );
      }.bind(this));
    }
    return (
      React.createElement("div", {className: "form-group", id: "market-preview"}, 
      React.createElement(ReactTransitionGroup, null, 
        markets
      )
      )
    );
  }
});

var MarketDialog = React.createClass({displayName: "MarketDialog",
  getInitialState: function() {
    return {};
  },

  parseMarkets: function() {
    // execute the parsing code
    var ps = $(React.findDOMNode(this)).find("#jsCode").val();
    var markets = [];
    eval(ps);

    // save the markets to the state so that admin can save it later
    this.setState({markets: markets});
  },

  saveMarkets: function() {
    // send the markets to server for saving
    requestJSON({
      url: "admin/market_save",
      method: "POST",
      data: {
        matchId: this.state.matchId,
        markets: JSON.stringify(this.state.markets),
        filter: this.props.parent.currentFilter()
      },
      success: function(json) {
        if (json.status == "ok") {
          this.props.parent.setState(json.data);
          this.hideDialog();
        } else {
          showError(json);
        }
      }.bind(this)
    });
  },

  clearPreview: function() {
    this.setState({markets: null})
  },

  showDialog: function(matchId) {
    var _state = this.state;
    _state = {matchId: matchId};
    this.replaceState(_state);

    $(React.findDOMNode(this)).modal({
      show: true
    });

    // get the saved parsing code
    $.ajax({
      url: "admin/parsing_code.js",
      method: "POST",
      type: "text",
      success: function(data) {
        $("#jsCode").val(data);
      }
    });
  },

  hideDialog: function() {
    $(React.findDOMNode(this)).modal("hide");
  },

  toggleNote: function() {
    $("pre.note").toggleClass("collapsed");
  },

  handleRemoveMarketItem: function(idx) {
    var _markets = this.state.markets;
    _markets.splice(idx, 1);
    this.setState({markets: _markets});
  },

  render: function() {
    var alertMsg = null;
    if (this.state.error) {
      alertMsg = React.createElement("div", {className: "alert alert-danger alert-xs"}, this.state.error);
    }
    return (
      React.createElement("div", {className: "modal", id: "market-modal", tabIndex: "-1", role: "dialog", "aria-labelledby": "modal-label"}, 
        React.createElement("div", {className: "modal-dialog modal-lg", role: "document"}, 
          React.createElement("div", {className: "modal-content"}, 
            React.createElement("div", {className: "modal-header"}, 
              React.createElement("button", {type: "button", className: "close", "data-dismiss": "modal", "aria-label": "Close"}, React.createElement("span", {"aria-hidden": "true"}, "×")), 
              React.createElement("h4", {className: "modal-title", id: "modal-label"}, "Market Creation Ultility")
            ), 
            React.createElement("div", {className: "modal-body"}, 
              alertMsg, 
              React.createElement("div", {className: "well"}, 
                React.createElement("form", null, 
                  React.createElement("div", {className: "row"}, 
                  React.createElement("div", {className: "form-group col-xs-6"}, 
                    React.createElement("label", {htmlFor: "htmlCode", className: "control-label"}, "HTML Code"), 
                    React.createElement("span", {className: "code-note"}, "(id=htmlCode - Paste HTML code here)"), 
                    React.createElement("textarea", {className: "form-control text-html", id: "htmlCode"})
                  ), 
                  React.createElement("div", {className: "form-group col-xs-6"}, 
                    React.createElement("label", {htmlFor: "jsCode", className: "control-label"}, "Parsing Code"), 
                    React.createElement("span", {className: "code-note"}, "(id=jsCode - Parsing code is loaded from admin/parsing_code.jsx)"), 
                    React.createElement("textarea", {className: "form-control text-js", id: "jsCode"})
                  )
                  ), 
                  React.createElement("div", {className: "form-group"}, 
                    React.createElement("pre", {className: "note collapsed", onClick: this.toggleNote}, "Note: (click to expand/collapse)", React.createElement("br", null), 
                    "After parsing, markets array should be filled with data in format:", 
                    "\n[\n  {\n    name: market_name,\n    odds: [\n      {name: odd_name, rate: odd_rate}\n    ]\n  }\n]"
                    )
                  ), 
                  React.createElement(Markets, {markets: this.state.markets, onRemoveItem: this.handleRemoveMarketItem})
                )
              )
            ), 
            React.createElement("div", {className: "modal-footer"}, 
              React.createElement("button", {className: "btn btn-default", disabled: !this.state.markets || this.state.markets.length == 0, onClick: this.clearPreview}, "Clear Preview"), 
              React.createElement("button", {className: "btn btn-success", onClick: this.parseMarkets}, "Parse"), 
              React.createElement("button", {className: "btn btn-primary", disabled: !this.state.markets || this.state.markets.length == 0, onClick: this.saveMarkets}, "Save Markets")
            )
          )
        )
      )
    );
  }
});