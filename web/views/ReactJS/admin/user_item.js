var User = React.createClass({displayName: "User",
  handleEditUserClick: function(e) {
    this.props.onEditUser({user: this.props.user});
  },
  
  handleDeleteUserClick: function(e) {
    this.props.onDeleteUser({userId: this.props.user._id});
  },
  
  render: function() {
    return (
      React.createElement("tr", {ref: "rowElement"}, 
        React.createElement("td", {id: "fbId"}, this.props.user.fbId), 
        React.createElement("td", {id: "userName"}, this.props.user.userName), 
        React.createElement("td", {id: "email"}, this.props.user.email), 
        React.createElement("td", {id: "initPoint"}, this.props.user.initPoint), 
        React.createElement("td", {id: "userType", "data-val": this.props.user.userType}, this.props.userType), 
        React.createElement("td", {className: "text-center text-nowrap"}, 
          React.createElement("button", {type: "button", className: "btn btn-success btn-sm", id: "edit-user", title: "Edit", onClick: this.handleEditUserClick}, 
            React.createElement("span", {className: "glyphicon glyphicon-edit", "aria-hidden": "true"})
          ), " ", 
          React.createElement("button", {type: "button", className: "btn btn-danger btn-sm", id: "delete-user", title: "Delete", onClick: this.handleDeleteUserClick}, 
            React.createElement("span", {className: "glyphicon glyphicon-trash", "aria-hidden": "true"})
          )
        )
      )
    );
  }
});