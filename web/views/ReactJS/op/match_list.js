var MatchList = React.createClass({displayName: "MatchList",

  getInitialState: function() {
    return {matches: []};
  },
  
  currentFilter: function() {
    return JSON.stringify(this.state.filter);
  },
  
  requestData: function(filter) {
    if (!filter) {
      filter = this.currentFilter();
    } else {
      filter = JSON.stringify(filter);
    }
    requestJSON({
      url: "op/match_list_data",
      method: "POST",
      data: {filter: filter},
      success: function(json) {
        if (json.status == "ok") {
          this.setState(json.data);
          // tell the time filter panel to update its date pickers
          this.refs.timeFilterPanel.update(this.state.filter);
        } else {
          showError(json);
        }
      }.bind(this)
    });
  },
  
  componentDidMount: function() {
    this.requestData();
  },
  
  render: function() {
    var matchDialog = null;
    if (this.state.matchDialog) {
      matchDialog = this.state.matchDialog.render();
    }
    
    var marketDialog = null;
    if (this.state.marketDialog) {
      marketDialog = this.state.marketDialog.render();
    }
    
    var buttonAdd = null;
    if (this.state.buttonAdd) {
      buttonAdd = this.state.buttonAdd.render();
    }
    
    var matches = this.state.matches.map(function (match, index) {
      return (
        React.createElement(Match, {key: match._id, index: index, parent: this, match: match})
      );
    }, this);
    
    return (
      React.createElement("div", {className: "multiple-components"}, 
        matchDialog, 
        marketDialog, 
        React.createElement(TimeFilterPanel, {parent: this, ref: "timeFilterPanel"}), 
        React.createElement("table", {className: "table table-striped table-bordered"}, 
          React.createElement("thead", null, 
            React.createElement("tr", null, 
              React.createElement("th", {className: "text-center"}, "Match Time"), 
              React.createElement("th", {className: "text-center"}, "Match"), 
              React.createElement("th", {className: "text-center match-result"}, "Result"), 
              React.createElement("th", {className: "text-center"}, "Actions ", buttonAdd)
            )
          ), 
          React.createElement("tbody", null, 
            matches
          )
        )
      )
    );
  },
});

pageComponent.setState({
  title: "Match List",
  pageContent: {
    render: function() {
      return React.createElement(MatchList, {ref: "matchList"});
    },
  }
});