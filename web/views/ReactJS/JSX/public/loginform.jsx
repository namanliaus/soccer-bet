'use strict'

var LoginForm = React.createClass({
  getInitialState: function() {
    return ({});
  },
  
  handleFBLogin: function(e) {
    e.preventDefault();
    FBLogin();
  },
  
  handleEmailLogin: function(e) {
    e.preventDefault();
    logIntoApp({
      email: $('.form-signin input#email').val(),
      password: $('.form-signin input#password').val()
    });
  },
  
  render: function() {
    var alertMsg;
    if (this.props.error) {
      alertMsg = <div className="alert alert-danger alert-xs">{this.props.error}</div>;
    }
    
    return (
      <div className="well well-login">
        {alertMsg}
        <button id="fb-login" className="btn btn-fb btn-block" onClick={this.handleFBLogin}><i className="fa fa-facebook"></i> Login with Facebook</button>
        <div className="socialSplitter">
          <hr />
          <span>OR sign in</span>
        </div>
        <form className="form-signin">
          <div className="form-group">
            <label htmlFor="email" className="sr-only">Email</label>
            <input type="email" className="form-control" id="email" required placeholder="Email" />
          </div>
          <div className="form-group">
            <label htmlFor="password" className="sr-only">Password</label>
            <input type="password" className="form-control" id="password" required placeholder="Password" />
          </div>
          {/*<div className="checkbox">
            <label>
              <input type="checkbox" /> Remember me
            </label>
          </div>*/}
          <button id="submit" className="btn btn-primary btn-block" onClick={this.handleEmailLogin}>Sign in</button>
        </form>
      </div>
    );
  }
});