'use strict'

var NavButton = React.createClass({
  render: function() {
    return (
      <button type="button" className={this.props.className} data-ref={this.props['data-ref']} onClick={this.props.onClick}/>
    );
  }
});

var NavBar = React.createClass({
  render: function() {
    var navBtnLeft = null;
    var navBtnRight = null;
    if (pageComponent && pageComponent.refs.menuPanel) {
      navBtnLeft = <NavButton className="visible-xs-inline-block btn btn-primary btn-panel navbar-btn glyphicon glyphicon-menu-right" id="btnShow-Menu" data-ref="#left-panel" ref="leftBtn" onClick={pageComponent.showPanel}/>;
    }
    if (pageComponent && pageComponent.refs.slipPanel) {
      navBtnRight = <NavButton className="visible-xs-inline-block btn btn-primary btn-panel navbar-btn glyphicon glyphicon-menu-left navbar-btn-right" id="btnShow-SlipList" data-ref="#right-panel" ref="rightBtn" onClick={pageComponent.showPanel}/>;
    }
    return (
      <nav className="navbar navbar-default navbar-fixed-top" id="navbar">
        <div className="row">
          <div className="col-xs-2">
            {navBtnLeft}
          </div>
          <div className="col-xs-8" id="page-title">
            <h3 className="navbar-header text-center">{this.props.title}</h3>
          </div>
          <div className="col-xs-2">
            {navBtnRight}
          </div>
        </div>
      </nav>
    );
  }
});
