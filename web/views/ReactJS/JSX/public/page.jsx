'use strict'

var Page = React.createClass({
  getInitialState: function() {
    return ({
      title: "HC Soccer Betting Game",
      matches: null,
      slips: null,
      indexedSlips: {},
      userList: []
    });
  },

  hidePanel: function(panelID) {
    // @BUG: if using mask with "click touch" handler, on mobile device, when
    // I want to focus in a slip list input, the event emits and the panel is hidden.
    // For now, implement clicking the header or menu item to hide panel.

    var mask = $('.modal-backdrop.in');
    var panelID = mask.attr('data-ref');
    if (!panelID) {
      return;
    }
    $(panelID).css(panelID.match(/#(.+)-/)[1], -$(panelID).width() - 5);
    //$("#" + panelID).css(panelID.split("-")[0], -$(panelID).width() - 5);
    $(panelID).addClass('hidden-xs');
    mask.remove();
  },

  showPanel: function(e) {
    // @BUG: if using mask with "click touch" handler, on mobile device, when
    // I want to focus in a slip list input, the event emits and the panel is hidden.
    // For now, implement clicking the header or menu item to hide panel.

    var btn = $(e.target);
    var panelID = btn.attr('data-ref');
    $(panelID).removeClass('hidden-xs');
    $(panelID).css(panelID.match(/#(.+)-/)[1], 0);
    $('<div class="modal-backdrop in" data-ref="' + panelID + '"></div>')
    .appendTo($('body'))
    .on('touch click', function() {
      this.hidePanel();
    }.bind(this));
  },

  render: function() {
    var content = null;
    if (this.state.loggedin === true) {
      var slipPanel = null;
      var menuPanel = null;

      // show slipPanel if normal player is logging in and there are slips to show.
      if (this.state.slipPanel) {
        slipPanel = this.state.slipPanel.render();
      }

      if (this.state.menuPanel) {
        menuPanel = this.state.menuPanel.render(this.state.menuitems);
      }

      var pageContent = null;
      if (this.state.pageContent) {
        pageContent = this.state.pageContent.render();
      }

      // if slipPanel is about to show then the content area becomes smaller
      var contentWidth = "col-xs-12 ";
      if (slipPanel) {
        contentWidth += "col-sm-6 col-md-8";
      } else {
        contentWidth += "col-sm-9 col-md-10";
      }

      content = (
        <div>
          {menuPanel}
          <div id="content" className={contentWidth}>
            {pageContent}
          </div>
          {slipPanel}
        </div>
      );
    } else if (this.state.loggedin === false) {
      // render login form instead
      content = (
        <div id="content">
          <LoginForm error={this.state.error} ref="loginForm"/>
        </div>
      )
    }

    return (
      <div>
        <NavBar title={this.state.title} parent={this}/>
        {content}
      </div>
    )
  }
});

var pageComponent = null;
$(function() {
  pageComponent = React.render(
    <Page />,
    document.getElementById("page")
  );
});