var MatchList = React.createClass({

  getInitialState: function() {
    return {matches: []};
  },
  
  currentFilter: function() {
    return JSON.stringify(this.state.filter);
  },
  
  requestData: function(filter) {
    if (!filter) {
      filter = this.currentFilter();
    } else {
      filter = JSON.stringify(filter);
    }
    requestJSON({
      url: "op/match_list_data",
      method: "POST",
      data: {filter: filter},
      success: function(json) {
        if (json.status == "ok") {
          this.setState(json.data);
          // tell the time filter panel to update its date pickers
          this.refs.timeFilterPanel.update(this.state.filter);
        } else {
          showError(json);
        }
      }.bind(this)
    });
  },
  
  componentDidMount: function() {
    this.requestData();
  },
  
  render: function() {
    var matchDialog = null;
    if (this.state.matchDialog) {
      matchDialog = this.state.matchDialog.render();
    }
    
    var marketDialog = null;
    if (this.state.marketDialog) {
      marketDialog = this.state.marketDialog.render();
    }
    
    var buttonAdd = null;
    if (this.state.buttonAdd) {
      buttonAdd = this.state.buttonAdd.render();
    }
    
    var matches = this.state.matches.map(function (match, index) {
      return (
        <Match key={match._id} index={index} parent={this} match={match} />
      );
    }, this);
    
    return (
      <div className="multiple-components">
        {matchDialog}
        {marketDialog}
        <TimeFilterPanel parent={this} ref="timeFilterPanel"/>
        <table className="table table-striped table-bordered">
          <thead>
            <tr>
              <th className="text-center">Match Time</th>
              <th className="text-center">Match</th>
              <th className="text-center match-result">Result</th>
              <th className="text-center">Actions {buttonAdd}</th>
            </tr>
          </thead>
          <tbody>
            {matches}
          </tbody>
        </table>
      </div>
    );
  },
});

pageComponent.setState({
  title: "Match List",
  pageContent: {
    render: function() {
      return <MatchList ref="matchList"/>;
    },
  }
});