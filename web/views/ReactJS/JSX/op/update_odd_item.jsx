var OddItem = React.createClass({
  
  handleOddClick: function() {
    var oddId = this.props.data._id;
    
    var _state = {};
    $.extend(_state, pageComponent.refs.matchUpdater.state.winItems);
    // there is an item?
    if (_state[oddId]) {
      // toggle its winning state
      _state[oddId].isWinner = !_state[oddId].isWinner
    } else {
      _state[oddId] = {marketId: this.props.marketId, isWinner: !this.props.data.isWinner};
    }
    
    pageComponent.refs.matchUpdater.setState({winItems: _state});
  },
  
  render: function() {
    var selectionStyle = "plain";
    if (this.props.slip && this.props.slip.isWinner) {
      selectionStyle = "won";
    }

    return (
      // with click handler
      <div className={"col-xs-" + this.props.gridSize + " odd-cell"}>
        <div className={"odd " + selectionStyle} onClick={this.handleOddClick}>
          <div className="odd-name">{this.props.data.name}</div>
        </div>
      </div>
    );
  }
});