var Match = React.createClass({
  getInitialState: function() {
    return {
      isEditing: false
    };
  },

  editResult: function() {
    requestJSON({
      url: "op/update_result_ui",
      method: "POST",
      data: {matchId: this.props.match._id},
      success: function(json) {
        var list = (json.data.requiredFiles) ? json.data.requiredFiles.slice() : [];
        var script = "";

        for (var v in json.data.vars) {
          script += "var " + v + " = '" + json.data.vars[v] + "';\n";
        }

        fetchScripts(list, script);
      }.bind(this)
    });
  },

  render: function() {
    var buttons = null;
    if (this.props.parent.state.buttons) {
      buttons = this.props.parent.state.buttons.render(this);
    }

    return (
      <tr ref="rowElement">
        <td className="text-center" id="match-date">{moment.unix(this.props.match.time).local().format(DATETIME_FORMAT)}</td>
        <td className="text-center" id="match-name">{this.props.match.name}</td>
        <td className="text-center" id="match-result">{this.props.match.result}</td>
        <td className="text-center text-nowrap col-xs-1">
          {buttons}
          <button type="button" className="btn btn-default btn-sm" id="update-result" title="Update Result" onClick={this.editResult}>
            <span className="glyphicon glyphicon-pencil" aria-hidden="true"></span>
          </button>
        </td>
      </tr>
    );
  },
});