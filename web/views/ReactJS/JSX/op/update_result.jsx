var UpdateResultComponent = React.createClass({
  handleChange: function(e) {
    this.setState({result: e.target.value});
  },
  
  handleSave: function() {
    requestJSON({
      url: "op/update_result_save",
      data: {
        wonOdds: JSON.stringify(this.state.winItems),
        matchId: this.props.match._id,
        result: this.state.result
      },
      method: "POST",
      success: function(json) {
        if (json.status == "ok") {
          this.setState({alert: "Match result saved."});
          this.hideAlert();
        } else {
          showError(json);
        }
      }.bind(this)
    });
  },
  
  hideAlert: function() {
    setTimeout(function() {
      this.setState({alert: null});
    }.bind(this), 2000);
  },
  
  getInitialState: function() {
    return {winItems: {}};
  },
  
  shouldComponentUpdate: function(nextProps, nextState) {
    if (this.state.alert != nextState.alert) {
      return true;
    }
    
    // //change in result the first time should force re-rendering
    // if (this.state.result === undefined && nextState.result !== undefined) {
      // return true;
    // }
    // no change in odd selecting, should not re-render
    if (this.state.winItems == nextState.winItems) {
      return false;
    }
    
    return true;
  },
  
  componentDidMount: function() {
    // update the winItems state at the first time mounting
    var winItems = {};
    for (var i = 0; i < this.props.match.markets.length; i++) {
      var market = this.props.match.markets[i];
      for (var j = 0; j < market.odds.length; j++) {
        var odd = market.odds[j];
        if (odd.isWinner) {
          winItems[odd._id] = {marketId: market._id, isWinner: true};
        }
      }
    }

    this.setState({
      result: this.props.match.result,
      winItems: winItems
    });
  },
  
  render: function() {
    var alertMsgTop = null, alertMsgBottom = null;
    if (this.state.alert) {
      alertMsgTop = <div className="alert alert-info">{this.state.alert}</div>;
      alertMsgBottom = <div className="alert alert-info">{this.state.alert}</div>;
    }
    return (
      <div>
        {alertMsgTop}
        <div className="row update-match-result">
          <div className="col-xs-6 text-right">
            Score of the match
          </div>
          <div className="col-xs-6">
            <input className="form-control input-md input-match-result" type="text" value={this.state.result} onChange={this.handleChange}/>
          </div>
        </div>
        <button className="btn btn-success form-control" onClick={this.handleSave}>Save</button>
        
        <MatchItem match={this.props.match} slipList={{winItems: this.state.winItems}} />
        <hr/>
        {alertMsgTop}
        <button className="btn btn-success form-control" onClick={this.handleSave}>Save</button>
      </div>
    );
  }
});

requestJSON({
  url: "op/update_result_data",
  data: {matchId: updatingMatchId},
  method: "POST",
  success: function(json) {
    pageComponent.setState({
      title: "Update Match Result",
      pageContent: {
        render: function() {
          return <UpdateResultComponent match={json.data.match} ref="matchUpdater"/>;
        }
      }
    })
  }
})