// request list of matches
var matchListURL = "player/match_list_data";
requestJSON({
  url: matchListURL,
  method: "POST",
  data: {filter: null},
  success: function(json) {
    // console.log(json);
    if (json.status == "ok") {
      pageComponent.setState({
        title: "Match List",
        matches: json.data.matches,
        timeFilter: json.data.filter,
        pageContent: {
          render: function() {
            return <MatchList dataURL={matchListURL} timeFilter={{showPast: false, showFuture: true}} matches={pageComponent.state.matches} slips={pageComponent.state.indexedSlips} ref="matchList"/>;
          },
        }
      });
    } else {
      showError(json);
    }
  }
});