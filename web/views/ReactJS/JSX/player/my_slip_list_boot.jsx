
var mySlipListURL = "player/my_slip_list_data";
requestJSON({
  url: mySlipListURL,
  method: "POST",
  success: function(json) {
    // console.log(json);
    if (json.status == "ok") {
      pageComponent.setState({
        title: "My Slips",
        matches: json.data.matches,
        timeFilter: json.data.filter,
        pageContent: {
          render: function() {
            return <MatchList dataURL={mySlipListURL} timeFilter={{showPast: true, showFuture: true}} matches={pageComponent.state.matches} slips={pageComponent.state.indexedSlips} ref="matchList"/>;
          },
        }
      });
    } else {
      showError(json);
    }
  }
});