var OddItem = React.createClass({

  handleOddClick: function() {
    pageComponent.refs.matchList.handleOddClick({
      matchId: this.props.matchId,
      marketId: this.props.marketId,
      oddId: this.props.data._id,
      slipId: (this.props.slip) ? this.props.slip._id : null,
      slip: this.props.slip
    });
  },

  getSelectionState: function() {

    var result = {
      selectionStyle: "plain",
      betAmount: 0,
      wonValue: 0,
      future: true
    };

    var now = moment().utc().unix();

    // console.log(this.props);
    if (this.props.slip) {
      // future slip
      result.betAmount = this.props.slip.betAmount;
      result.wonValue = this.props.slip.won;
      result.matchTime = this.props.slip.matchTime;
    } else if (this.props.data.matchTime && this.props.data.matchTime < now) {
      // past slip
      result.matchTime = this.props.data.matchTime;
      result.betAmount = this.props.data.betAmount;
      result.wonValue = this.props.data.won;
    }

    if (result.matchTime > now) {
      // slips for future matches
      if (result.betAmount == 0) result.selectionStyle = "wait";
      else result.selectionStyle = "bet";
    } else if (result.matchTime) {
      result.future = false;
      if (result.wonValue > 0) result.selectionStyle = "won";
      else result.selectionStyle = "lost";
    }

    return result;
  },

  render: function() {
    var ss = this.getSelectionState();

    var displayAmount = null;
    if (ss.betAmount > 0) {
      displayAmount = ss.betAmount;
      if (ss.selectionStyle == "won") {
        displayAmount = roundFloat2dp(ss.wonValue) + "/" + roundFloat2dp(displayAmount);
      }
    }

    if (!ss.future) {
      var userName = null;
      if (this.props.slip && this.props.slip.userId) {
        userName = this.props.slip.userId.userName;
      }
      return (
        // no click handler
        <div className={"col-xs-" + this.props.gridSize + " odd-cell"}>
          <div className="player-name">{userName}</div>
          <div className={"odd " + ss.selectionStyle}>
            <div className="odd-name">{this.props.data.name}</div>
            <div className="odd-rate">{this.props.data.rate}</div>
            <div className="bet-amount">{displayAmount}</div>
          </div>
        </div>
      );
    } else {
      return (
        // with click handler
        <div className={"col-xs-" + this.props.gridSize + " odd-cell"}>
          <div className={"odd " + ss.selectionStyle} onClick={this.handleOddClick}>
            <div className="odd-name">{this.props.data.name}</div>
            <div className="odd-rate">{this.props.data.rate}</div>
            <div className="bet-amount">{displayAmount}</div>
          </div>
        </div>
      );
    }
  }
});