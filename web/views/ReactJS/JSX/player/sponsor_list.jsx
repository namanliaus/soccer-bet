var SponsorItem = React.createClass({
  render: function() {
    return (
      <tr ref="rowElement">
        <td className="text-center">{this.props.index + 1}</td>
        <td className="text-center">{this.props.sponsor.userName}</td>
        <td className="text-center">x{this.props.sponsor.sponsor}</td>
      </tr>
    );
  }
});

var SponsorList = React.createClass({
  getInitialState: function() {
    return {};
  },

  handlerSponsor: function() {
    showModalDialog({
      type: "info",
      title: "Please confirm",
      content: "Thank you for your sponsorship. Once you submit, you won't be able to revoke it.",
      btn1: {
        text: "OK",
        onclick: function() {
          requestJSON({
            url: "player/sponsor_add",
            method: "POST",
            success: function(json) {
              if (json.status == "ok") {
                pageComponent.setState({sponsorList: json.data});
              } else {
                showError(json);
              }
            }
          });
        }
      },
      btn2: {text: "Cancel"}
    });
  },

  render: function() {
    var sponsors = this.props.sponsors.map(function(sponsor, idx) {
      return <SponsorItem key={sponsor._id} index={idx} sponsor={sponsor} />
    });

    var button = null;
    if (this.state.sponsorButton) {
      button = this.state.sponsorButton.render();
    }

    return (
      <div className="multiple-components">
        <table className="table table-striped table-bordered">
          <thead>
            <tr>
              <th className="text-center"></th>
              <th className="text-center">Name</th>
              <th className="text-center">Times</th>
            </tr>
          </thead>
          <tbody>
            {sponsors}
          </tbody>
        </table>
        {button}
      </div>
    );
  }
});

requestJSON({
  url: "player/sponsor_list_data",
  method: "POST",
  success: function(json) {
    if (json.status == "ok") {
      pageComponent.setState({
        title: "Sponsors",
        sponsorList: json.data,
        pageContent: {
          render: function() {
            return <SponsorList sponsors={pageComponent.state.sponsorList} ref="sponsorList" />
          }
        }
      });
    } else {
      showError(json);
    }
  }
})