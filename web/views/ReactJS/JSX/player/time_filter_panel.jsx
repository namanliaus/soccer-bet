var TimeFilterPanel = React.createClass({
  
  componentDidMount: function() {
    // initialize the datetime elements
    var $elStart = $("#dtp_start");
    $elStart.datetimepicker({
      format: DATETIME_FORMAT,
      locale: "en"
    });
    this.$elStart = $elStart;
    
    var $elEnd = $("#dtp_end");
    $elEnd.datetimepicker({
      format: DATETIME_FORMAT,
      locale: "en"
    });
    this.$elEnd = $elEnd;
    
    // not for the past date?
    if (!this.props.past) {
      // limit max time to now
      this.$elStart.data("DateTimePicker").minDate(moment());
      this.$elEnd.data("DateTimePicker").minDate(moment());
    }
    
    // not for the future date?
    if (!this.props.future) {
      // limit max time to now
      this.$elStart.data("DateTimePicker").maxDate(moment());
      this.$elEnd.data("DateTimePicker").maxDate(moment());
    }
    
    // start must ealier than end
    this.$elStart.on("dp.change", function(e) {
      this.$elEnd.data("DateTimePicker").minDate(e.date);
    }.bind(this));
    this.$elEnd.on("dp.change", function(e) {
      this.$elStart.data("DateTimePicker").maxDate(e.date);
    }.bind(this));

    // update the data from the top level
    this.update(pageComponent.state.timeFilter);
  },
  
  update: function(filter) {
    // yesterday or the value from the filter start time
    var mmS = moment().subtract(1, "d");
    if (filter && filter.start_time) {
      mmS = moment.unix(filter.start_time).local();
    }
    this.$elStart.data("DateTimePicker").date(mmS);
    
    // tomorrow of the filter end time
    var mmE = moment().add(1, "d");
    if (filter && filter.end_time) {
      mmE = moment.unix(filter.end_time).local();
    }
    this.$elEnd.data("DateTimePicker").date(mmE);
    
    this.$elStart.data("DateTimePicker").maxDate(mmE);
    this.$elEnd.data("DateTimePicker").minDate(mmS);
  },
  
  setFilter: function() {
    // validate date time
    var mms = this.$elStart.data("DateTimePicker").date();
    var mme = this.$elEnd.data("DateTimePicker").date();
    
    if (!mms || !mme) {
      showError({message: "Invalid date formats. Please check."});
      return;
    }
    
    // ready to submit
    var filter = {
      start_time: mms.utc().unix(),
      end_time: mme.utc().unix()
    }

    this.props.parent.requestData(filter);
  },
  
  getDefaultProps: function() {
    return {
      future: true,
      past: true
    };
  },
  
  render: function() {
    return (
      <div className="well">
        <div className="row">
          <div className="col-xs-5">
            <div className="form-group no-margin-bottom">
              <label htmlFor="filter-startdate" className="control-label">From Date</label>
              <input id="dtp_start" type="text" placeholder={DATETIME_FORMAT} className="form-control"/>
            </div>
          </div>
          <div className="col-xs-5">
            <div className="form-group no-margin-bottom">
              <label htmlFor="filter-enddate" className="control-label">To Date</label>
              <input type="text" id="dtp_end" placeholder={DATETIME_FORMAT} className="form-control"/>
            </div>
          </div>
          <div className="col-xs-2">
            <button className="btn btn-default btn-controlpanel-two-rows" onClick={this.setFilter}>
              <span className="glyphicon glyphicon-filter"></span> Filter
            </button>
          </div>
        </div>
      </div>
    );
  }
});