var SponsorButton = React.createClass({
  render: function() {
    return (
      <button className="btn btn-primary form-control" onClick={this.handlerSponsor}>One click will add one time sponsoring</button>
    );
  }
});

function waitForSponsorListReady() {
  var sponsorList = pageComponent.refs.sponsorList;

  if (!sponsorList) {
    setTimeout(waitForSponsorListReady, 100);
    return;
  }

  sponsorList.setState({
    sponsorButton: {
      render: function() {
        return <SponsorButton />;
      }
    }
  });
}

waitForSponsorListReady();