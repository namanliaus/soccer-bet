var MenuPanel = React.createClass({
  handleItemClick: function (e) {
    e.preventDefault();
    this.executeAction($(e.target).attr("data-ref"));
    pageComponent.hidePanel("#left-panel");
  },

  render: function() {
    var items = [];
    var idx = 0;
    if (this.props.items) {
      items = this.props.items.map(function (item) {
        idx++;
        if (item.type == "header") {
          return (
            <div key={idx} className="list-group-item active text-center" onClick={pageComponent.hidePanel.bind(null, "#left-panel")}>
              <div className="menu-username">{item.username}</div>
            </div>
          );
        } else if (item.type == "divider") {
          return <div key={idx} className="list-group-item list-group-divider"></div>;
        } else if (item.type == "link") {
          if (item.style) {
            return <a key={idx} href="#" className={"list-group-item list-group-item-"+item.style} data-ref={item.action} onClick={this.handleItemClick}>{item.text}</a>
          } else {
            return <a key={idx} href="#" className="list-group-item" data-ref={item.action} onClick={this.handleItemClick}>{item.text}</a>
          }
        }
      }.bind(this));
    }
    return (
      <div className="hidden-xs col-xs-3 col-md-2 list-group" id="left-panel">
        {items}
      </div>
    );
  },

  executeAction: function(action) {
    if (action === "logout") {
      logOut();
    } else {
      // make a request to server and then load the content base on json response
      requestJSON({
        url: action + "_ui",
        method: "POST",
        success: function(json) {
          var list = (json.data.requiredFiles) ? json.data.requiredFiles.slice() : [];
          fetchScripts(list);
        }.bind(this)
      });

      // let's take this chance to update user points
      if (pageComponent.refs.slipPanel && pageComponent.refs.slipPanel.refs.userPointComponent) {
        pageComponent.refs.slipPanel.refs.userPointComponent.requestData();
      }
    }
  }
});

pageComponent.setState({
  menuPanel: {
    render: function(items) {
      return <MenuPanel items={items} ref="menuPanel"/>;
    }
  }
});