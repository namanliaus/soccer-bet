var SlipItem = React.createClass({

  handleRemove: function() {
    var _data = {
      slip: JSON.stringify({
        matchId: this.props.data.matchId,
        marketId: this.props.data.marketId,
        oddId: this.props.data.oddId,
        slipId: this.props.data._id
      })
    };

    pageComponent.refs.slipPanel.sendRequestAndUpdate(_data);
  },

  handleChangeBetAmount: function(e) {

    var val = e.target.value;

    // the number input auto filters the strings so when user enters an invalid char
    // val is an empty string.
    if (val.length == 0) {
      this.setState({
        estimate: "",
        betAmount: ""
      });
      e.target.value = "";
      return;
    }

    // cannot be zero or negative
    if (parseFloat(val) <= 0.0) {
      this.setState({estimate: ""});
      return;
    }

    this.setState({
      betAmount: val,
      estimate: roundFloat2dp(this.props.data.oddRate * val)
    });
  },

  handleBet: function() {
    var _data = {
      slip: JSON.stringify({
        matchId: this.props.data.matchId,
        marketId: this.props.data.marketId,
        oddId: this.props.data.oddId,
        slipId: this.props.data._id,
        betAmount: this.state.betAmount
      })
    };

    pageComponent.refs.slipPanel.sendRequestAndUpdate(_data);
  },

  getInitialState: function() {
    return {
      betAmount: "",
      estimate: ""
    }
  },

  render: function() {
    return (
      <div className="list-group-item slip-item">
        <div>
          <div className="slip-button-close">
            <button type="button" className="btn btn-sm btn-danger slip-button-close" onClick={this.handleRemove}>
              <span className="glyphicon glyphicon-remove slip-button-close"></span>
            </button>
          </div>
          <span className="slip-match">{this.props.data.matchName}</span>
          <span className="slip-match-time"> {moment.unix(this.props.data.matchTime).local().format(DATETIME_FORMAT)}</span>
        </div>
        <div className="slip-market">{this.props.data.marketName} - {this.props.data.oddName} @{this.props.data.oddRate}</div>
        <div className="row">
          <div className="col-xs-9 slip-bet-amount">
            <div className="input-group">
              <span className="input-group-addon slip-estimate-number">{this.state.estimate}</span>
              <input type="number" className="form-control input-sm" size="6" min="0" place-holder="Bet Amount" value={this.state.betAmount} onChange={this.handleChangeBetAmount}/>
            </div>
          </div>
          <div className="col-xs-3 slip-button-save">
            <button type="button" className="btn btn-sm btn-success form-control slip-button-save" onClick={this.handleBet} disabled={!this.state.betAmount || this.state.betAmount < 1}>
              <span className="glyphicon glyphicon-ok"></span>
            </button>
          </div>
        </div>
      </div>
    )
  }
});