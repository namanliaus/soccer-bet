var ChangePasswordComponent = React.createClass({
  handleChange: function(e) {
    var _state = {};
    $.extend(_state, this.state.values);
    _state[e.target.id] = e.target.value;
    
    var stateToSet = {values: _state};
    if (_state.newPassword === _state.newPasswordAgain) {
      stateToSet.status = {
        feedbackClass: " has-success has-feedback",
        feedbackItem: {
          render: function() {
            return (
              <span className="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
            );
          }
        },
        feedbackText: "(success)"
      }
    } else {
      stateToSet.status = {
        feedbackClass: " has-error has-feedback",
        feedbackItem: {
          render: function() {
            return (
              <span className="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
            );
          }
        },
        feedbackText: "(password not match)"
      }
    }
    
    this.setState(stateToSet);
  },
  
  getInitialState: function() {
    return {
      values: {
        oldPassword: "",
        newPassword: "",
        newPasswordAgain: ""
      },
      status: {
        feedbackClass: "",
        feedbackItem: null,
        feedbackText: ""
      }
    };
  },
  
  handleSubmit: function(e) {
    e.preventDefault();
    requestJSON({
      url: "player/settings_changepassword",
      method: "POST",
      data: {
        oldPassword: this.state.values.oldPassword,
        newPassword: this.state.values.newPassword
      },
      success: function(json) {
        if (json.status == "ok") {
          showModalDialog({
            type: "info",
            title: "Change password",
            content: "Password changed successfully",
            btn1: {text: "OK"}
          });
        } else {
          showError(json);
        }
      }
    })
  },
  
  render: function() {
    var feedbackItem = null;
    
    if (this.state.status.feedbackItem) {
      feedbackItem = this.state.status.feedbackItem.render();
    }
    
    return (
      <div className="well well-login">
        <h4>Change password</h4>
        <form className="form-signin">
          <div className="form-group">
            <label htmlFor="oldPassword" className="sr-only">Old Password</label>
            <input type="password" className="form-control" id="oldPassword" required placeholder="Old Password" onChange={this.handleChange} value={this.state.oldPassword}/>
          </div>
          <div className="form-group">
            <label htmlFor="newPassword" className="sr-only">Password</label>
            <input type="password" className="form-control" id="newPassword" required placeholder="New Password"  onChange={this.handleChange} value={this.state.newPassword}/>
          </div>
          <div className={"form-group" + this.state.status.feedbackClass}>
            <label htmlFor="newPasswordAgain" className="sr-only">Password Again</label>
            <input type="password" className="form-control" id="newPasswordAgain" required placeholder="New Password Again"  onChange={this.handleChange} value={this.state.newPasswordAgain} aria-describedby="inputStatus"/>
            {feedbackItem}
            <span id="inputStatus" className="sr-only">{this.state.status.feedbackText}</span>
          </div>
          <button id="submit" className="btn btn-primary btn-block" onClick={this.handleSubmit}>Change Password</button>
        </form>
      </div>
    );
  }
});