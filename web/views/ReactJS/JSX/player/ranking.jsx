var RankingItem = React.createClass({
  render: function() {
    return (
      <tr ref="rowElement">
        <td className="text-center">{this.props.index + 1}</td>
        <td className="text-center">{this.props.data.userName}</td>
        <td className="text-center">{roundFloat2dp(this.props.data.totalBet)}</td>
        <td className="text-center">{roundFloat2dp(this.props.data.totalWin)}</td>
        <td className="text-center">{roundFloat2dp(this.props.data.winRate)}</td>
      </tr>
    );
  }
});

var RankingList = React.createClass({
  componentDidMount: function() {
    requestJSON({
      url: "player/ranking_data",
      method: "POST",
      success: function(json) {
        if (json.status == "ok") {
          this.setState({ranking: json.data});
        } else {
          showError(json);
        }
      }.bind(this)
    });
  },

  render: function() {

    var items = null;

    if (this.state && this.state.ranking) {
      items = this.state.ranking.map(function(item, idx) {
        return <RankingItem key={item._id} index={idx} data={item} />
      });
    }

    return (
      <div className="multiple-components">
        <table className="table table-striped table-bordered">
          <thead>
            <tr>
              <th className="text-center"></th>
              <th className="text-center">Name</th>
              <th className="text-center">Bet</th>
              <th className="text-center">Won</th>
              <th className="text-center">Win Rate</th>
            </tr>
          </thead>
          <tbody>
            {items}
          </tbody>
        </table>
      </div>
    );
  }
});

pageComponent.setState({
  title: "Ranking",
  pageContent: {
    render: function() {
      return <RankingList ref="rankingList" />
    }
  }
});