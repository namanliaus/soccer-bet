var historyURL = "player/history_data"
requestJSON({
  url: historyURL,
  method: "POST",
  success: function(json) {
    if (json.status == "ok") {
      pageComponent.setState({
        title: "History",
        matches: json.data.matches,
        timeFilter: json.data.filter,
        pageContent: {
          render: function() {
            return <MatchList dataURL={historyURL} timeFilter={{showPast: true, showFuture: false}} matches={pageComponent.state.matches} slips={pageComponent.state.indexedSlips} ref="history"/>;
          },
        }
      });
    } else {
      showError(json);
    }
  }
});