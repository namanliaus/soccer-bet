var MatchItem = React.createClass({
  
  handleClick: function() {
    // expand/collapse match
    var $el = $(this.getDOMNode());
    if ($el.hasClass("collapsed")) {
      // expand to the saved height
      $el.css("height", $el.attr("data-height"));
      $el.on("webkitTransitionEnd transitionend", function(e) {
        $el.css("height", "auto");
        $el.off("webkitTransitionEnd transitionend");
      });
      $el.removeClass("collapsed");
    } else {
      // save the height before collapsing
      $el.attr("data-height", $el.outerHeight());
      $el.css("height", $el.outerHeight());
      setTimeout(function() {
        $el.css("height", collapsedMatchItemHeight);
      }, 1000/60);
      $el.addClass("collapsed");
    }
  },
  
  componentDidMount: function() {
    // collapse the match at the first load
    this.handleClick();
  },
  
  render: function() {
    var markets = [];
    markets = this.props.match.markets.map(function (market, idx) {
      return (
        <MarketItem market={market} key={market._id} matchId={this.props.match._id} slipList={this.props.slipList} />
      );
    }.bind(this));

    return (
      <div className="match-item">
        <div className="match-head" onClick={this.handleClick}>{this.props.match.name}
          <span className="match-head-time">{moment.unix(this.props.match.time).local().format(DATETIME_FORMAT)}</span>
        </div>
        {markets}
      </div>
    );
  }
});