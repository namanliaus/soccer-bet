var User = React.createClass({
  handleEditUserClick: function(e) {
    this.props.onEditUser({user: this.props.user});
  },
  
  handleDeleteUserClick: function(e) {
    this.props.onDeleteUser({userId: this.props.user._id});
  },
  
  render: function() {
    return (
      <tr ref="rowElement">
        <td id="fbId">{this.props.user.fbId}</td>
        <td id="userName">{this.props.user.userName}</td>
        <td id="email">{this.props.user.email}</td>
        <td id="initPoint">{this.props.user.initPoint}</td>
        <td id="userType" data-val={this.props.user.userType}>{this.props.userType}</td>
        <td className="text-center text-nowrap">
          <button type="button" className="btn btn-success btn-sm" id="edit-user" title="Edit" onClick={this.handleEditUserClick}>
            <span className="glyphicon glyphicon-edit" aria-hidden="true"></span>
          </button>&nbsp;
          <button type="button" className="btn btn-danger btn-sm" id="delete-user" title="Delete" onClick={this.handleDeleteUserClick}>
            <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
          </button>
        </td>
      </tr>
    );
  }
});