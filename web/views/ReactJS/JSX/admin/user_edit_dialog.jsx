var UserEditDialog = React.createClass({
  
  handleChanges: function(event) {
    this.props.onEdit(event);
  },
  
  handleSaveClick: function(e) {
    this.props.action();
  },
  
  componentDidMount: function() {
    $(this.getDOMNode())
    // the modal dialog may be closed in many ways
    // handle this event for cleanning data
    .on("hidden.bs.modal", function(e) {
      // inform parent that it is hiding
      this.props.onDialogHidden();
    }.bind(this))
    // show the dialog
    .modal({
      keyboard: true,
      show: true
    });
  },
  
  componentWillUnmount: function() {
    $(this.getDOMNode()).modal("hide");
  },
  
  render: function() {
    var options = this.props.options.map(function(opt, idx) {
      if (idx > 0) {
        return (<option value={idx} key={idx}>{opt}</option>);
      }
    });
    
    return (
      <div className="modal" id="user-modal" tabIndex="-1" role="dialog" aria-labelledby="modal-label">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title" id="modal-label">{this.props.title}</h4>
            </div>
            <div className="modal-body">
              <div className="well well-user">
                <form id="user-form" className="form-horizontal">
                  <div className="form-group">
                    <label htmlFor="fbId" className="col-sm-3 control-label">Facebook ID</label>
                    <div className="col-sm-9">
                      <input type="text" id="fbId" name="fbId" className="form-control" placeholder="Facebook ID" value={this.props.user.fbId} onChange={this.handleChanges}/>
                    </div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="email" className="col-sm-3 control-label">Email</label>
                    <div className="col-sm-9">
                      <input type="email" id="email" name="email" className="form-control" placeholder="Email" value={this.props.user.email} onChange={this.handleChanges}/>
                    </div>
                  </div>
                   <div className="form-group">
                    <label htmlFor="password" className="col-sm-3 control-label">Password</label>
                    <div className="col-sm-9">
                      <input type="password" id="password" name="password" className="form-control" placeholder="Password" value={this.props.user.password} onChange={this.handleChanges}/>
                    </div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="username" className="col-sm-3 control-label">User Name</label>
                    <div className="col-sm-9">
                      <input type="text" id="userName" name="username" className="form-control" placeholder="User Name" value={this.props.user.userName} onChange={this.handleChanges}/>
                    </div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="initpoint" className="col-sm-3 control-label">Init Point</label>
                    <div className="col-sm-9">
                      <input type="text" id="initPoint" name="initpoint" className="form-control" placeholder="Init Point" value={this.props.user.initPoint} onChange={this.handleChanges}/>
                    </div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="usertype" className="col-sm-3 control-label">User Type</label>
                    <div className="col-sm-9">
                      <select className="form-control" id="userType" name="usertype" value={(this.props.user.userType)?this.props.user.userType:3} onChange={this.handleChanges}>
                        {options}
                      </select>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" id="btn-save-user" onClick={this.handleSaveClick}>Save changes</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
