var ReactTransitionGroup = React.addons.TransitionGroup;

var OddItem = React.createClass({
  render: function() {
    return (
      <div className={"col-xs-" + this.props.gridSize + " odd-cell"}>
        <div className="odd">
          <div className="odd-name">{this.props.data.name}</div>
          <div className="odd-rate">{this.props.data.rate}</div>
        </div>
      </div>
    );
  }
});

var MarketItem = React.createClass({

  handleClick: function() {
    // pass the event up to owners
    this.props.onRemoveItem();
  },

  render: function() {
    var colCount = 0;
    if (this.props.market.odds.length % 3 == 0 || this.props.market.odds.length > 4) {
      colCount = 3;
    } else {
      colCount = 2;
    }
    var gridSize = 12 / colCount;

    // render odd items
    var odds = this.props.market.odds.map(function(odd, idx) {
      return <OddItem gridSize={gridSize} data={odd} key={idx}/>;
    });

    // render the market;
    return (
      <div className="market-item">
        <div className="market-head" onClick={this.handleClick}>{this.props.market.name}</div>
        <div className="row row-odds">
          {odds}
        </div>
      </div>
    );
  }
});

var Markets = React.createClass({
  handleRemoveItem: function(idx) {
    // pass the event up to owner
    this.props.onRemoveItem(idx);
  },

  render: function() {
    var markets = [];
    if (this.props.markets) {
      markets = this.props.markets.map(function (market, idx) {
        return (
          <AnimationItem transitionName="listitem-hide" key={"listitem-hide-"+market.name} numTransIn="1" numTransOut="2">
            <MarketItem market={market} key={market.name} onRemoveItem={this.handleRemoveItem.bind(null, idx)}/>
          </AnimationItem>
        );
      }.bind(this));
    }
    return (
      <div className="form-group" id="market-preview">
      <ReactTransitionGroup>
        {markets}
      </ReactTransitionGroup>
      </div>
    );
  }
});

var MarketDialog = React.createClass({
  getInitialState: function() {
    return {};
  },

  parseMarkets: function() {
    // execute the parsing code
    var ps = $(React.findDOMNode(this)).find("#jsCode").val();
    var markets = [];
    eval(ps);

    // save the markets to the state so that admin can save it later
    this.setState({markets: markets});
  },

  saveMarkets: function() {
    // send the markets to server for saving
    requestJSON({
      url: "admin/market_save",
      method: "POST",
      data: {
        matchId: this.state.matchId,
        markets: JSON.stringify(this.state.markets),
        filter: this.props.parent.currentFilter()
      },
      success: function(json) {
        if (json.status == "ok") {
          this.props.parent.setState(json.data);
          this.hideDialog();
        } else {
          showError(json);
        }
      }.bind(this)
    });
  },

  clearPreview: function() {
    this.setState({markets: null})
  },

  showDialog: function(matchId) {
    var _state = this.state;
    _state = {matchId: matchId};
    this.replaceState(_state);

    $(React.findDOMNode(this)).modal({
      show: true
    });

    // get the saved parsing code
    $.ajax({
      url: "admin/parsing_code.js",
      method: "POST",
      type: "text",
      success: function(data) {
        $("#jsCode").val(data);
      }
    });
  },

  hideDialog: function() {
    $(React.findDOMNode(this)).modal("hide");
  },

  toggleNote: function() {
    $("pre.note").toggleClass("collapsed");
  },

  handleRemoveMarketItem: function(idx) {
    var _markets = this.state.markets;
    _markets.splice(idx, 1);
    this.setState({markets: _markets});
  },

  render: function() {
    var alertMsg = null;
    if (this.state.error) {
      alertMsg = <div className="alert alert-danger alert-xs">{this.state.error}</div>;
    }
    return (
      <div className="modal" id="market-modal" tabIndex="-1" role="dialog" aria-labelledby="modal-label">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title" id="modal-label">Market Creation Ultility</h4>
            </div>
            <div className="modal-body">
              {alertMsg}
              <div className="well">
                <form>
                  <div className="row">
                  <div className="form-group col-xs-6">
                    <label htmlFor="htmlCode" className="control-label">HTML Code</label>
                    <span className="code-note">(id=htmlCode - Paste HTML code here)</span>
                    <textarea className="form-control text-html" id="htmlCode" />
                  </div>
                  <div className="form-group col-xs-6">
                    <label htmlFor="jsCode" className="control-label">Parsing Code</label>
                    <span className="code-note">(id=jsCode - Parsing code is loaded from admin/parsing_code.jsx)</span>
                    <textarea className="form-control text-js" id="jsCode" />
                  </div>
                  </div>
                  <div className="form-group">
                    <pre className="note collapsed" onClick={this.toggleNote} >Note: (click to expand/collapse)<br/>
                    After parsing, markets array should be filled with data in format:
                    {"\n[\n  {\n    name: market_name,\n    odds: [\n      {name: odd_name, rate: odd_rate}\n    ]\n  }\n]"}
                    </pre>
                  </div>
                  <Markets markets={this.state.markets} onRemoveItem={this.handleRemoveMarketItem}/>
                </form>
              </div>
            </div>
            <div className="modal-footer">
              <button className="btn btn-default" disabled={!this.state.markets || this.state.markets.length == 0} onClick={this.clearPreview}>Clear Preview</button>
              <button className="btn btn-success" onClick={this.parseMarkets}>Parse</button>
              <button className="btn btn-primary" disabled={!this.state.markets || this.state.markets.length == 0} onClick={this.saveMarkets}>Save Markets</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
});