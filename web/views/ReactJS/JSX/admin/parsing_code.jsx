var titles = [];
$($("#htmlCode").val()).find("h3.is-open a.left").each(function() {
  titles.push($(this).html().trim());
});
$($("#htmlCode").val()).find("section[data-sports-id]:has(h3.is-open)").each(function(idx) {
  var market = {name: titles[idx], odds: []};
  markets.push(market);
  var currentKey = "";
  $(this).find("div[class^='large-']").each(function() {
    if ($(this).hasClass("large-10")) {
      currentKey = $(this).find("li").html().trim();
    } else {
      var odd = {
        name: currentKey,
        rate: $(this).find("a").text().trim()
      };
      market.odds.push(odd);
    }
  });
});