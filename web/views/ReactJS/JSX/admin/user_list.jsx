var UserList = React.createClass({
  
  userTypes: [
    '',
    'Administrator',
    'Operator',
    'Player',
    'Unapproved'
  ],
  
  updateUser: function() {
    // send data to server and get a full list of users
    requestJSON({
      url: "admin/user_save",
      data: this.state.editingUser,
      dataType: "json",
      method: "POST",
      success: function(json) {
        this.processArrivedData(json);
      }.bind(this)
    });
  },
  
  deleteUser: function(userID) {
    requestJSON({
      url: "admin/user_delete",
      data: {id: userID},
      dataType: "json",
      method: "POST",
      success: function(json) {
        this.processArrivedData(json);
      }.bind(this)
    });
  },
  
  handleAddNewUser: function() {
    this.showUserDialog({title: "Add new user", user: {userType: 3}, action: this.updateUser});
  },
  
  handleEditUser: function(data) {
    this.showUserDialog({title: "Edit user", user: data.user, action: this.updateUser});
  },
  
  handleDeleteUser: function(data) {
    showModalDialog({
      type: "warning",
      title: "Confirmation",
      content: "Are you sure to delete this user?",
      btn1: {
        text: "OK",
        onclick: function() {
          this.deleteUser(data.userId);
        }.bind(this)
      },
      btn2: {
        text: "Cancel"
      }
    });
  },
  
  handleEditUserDetail: function(e) {
    var user = {};
    $.extend(user, this.state.editingUser);
    // modify and then set the changes back to editing user
    user[e.target.id] = e.target.value;
    this.setState({editingUser: user});
  },
  
  handleHideDialog: function() {
    this.setState({
      editingUser: null,
      userDialog: null
    })
  },
  
  showUserDialog: function(data) {
    // publish user data to dialog and show it
    if (!this.state.userDialog) {
      this.setState({
        editingUser: data.user,
        userDialog: {
          render: function() {
            return <UserEditDialog title={data.title} user={this.state.editingUser} action={data.action} onEdit={this.handleEditUserDetail} onDialogHidden={this.handleHideDialog} options={this.userTypes} ref="userEditDialog"/>
          }.bind(this)
        }
      });
    }
  },
  
  processArrivedData: function(json) {
    if (json.status == "ok" && json.code == "1002") {
      if (this.state.userDialog) {
        this.handleHideDialog();
      }
      pageComponent.setState({userList: json.data.users});
    } else {
      showError(json);
    }
  },
  
  getInitialState: function() {
    return {
      userDialog: null,
      editingUser: null
    };
  },
  
  render: function() {
    var users = null;
    if (this.props.users) {
      users = this.props.users.map(function (user) {
        return (
          <User key={user._id} parent={this} user={user} onEditUser={this.handleEditUser} onDeleteUser={this.handleDeleteUser} userType={this.userTypes[user.userType]}/>
        );
      }.bind(this));
    }
    
    var userDialog = null;
    if (this.state.userDialog) {
      userDialog = this.state.userDialog.render();
    }
    
    return (
      <div className="multiple-components">
        {userDialog}
        <div className="text-right well">
          {/*<button className="btn btn-default" id="btn-search-friend">Search friends</button>&nbsp;*/}
          <button className="btn btn-success" id="btn-add-user" onClick={this.handleAddNewUser}>Add new user</button>
        </div>
        <table className="table table-striped table-bordered">
          <thead>
            <tr>
              <th className="text-center">Facebook ID</th>
              <th className="text-center">Name</th>
              <th className="text-center">Email</th>
              <th className="text-center">Init Point</th>
              <th className="text-center">User Type</th>
              <th className="text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
            {users}
          </tbody>
        </table>
      </div>
    )
  }
});

requestJSON({
  url: "admin/user_list_data",
  method: "POST",
  dataType: "json",
  success: function(json) {
    if (json.status == "ok") {
      // console.log(json);
      pageComponent.setState({
        userList: json.data.users,
        title: "User List",
        pageContent: {
          render: function() {
            return <UserList users={pageComponent.state.userList} />
          }
        }
      });
    } else {
      showError(json);
    }
  }
});