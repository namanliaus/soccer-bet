'use strict'

var NavButton = React.createClass({displayName: "NavButton",
  render: function() {
    return (
      React.createElement("button", {type: "button", className: this.props.className, "data-ref": this.props['data-ref'], onClick: this.props.onClick})
    );
  }
});

var NavBar = React.createClass({displayName: "NavBar",
  render: function() {
    var navBtnLeft = null;
    var navBtnRight = null;
    if (pageComponent && pageComponent.refs.menuPanel) {
      navBtnLeft = React.createElement(NavButton, {className: "visible-xs-inline-block btn btn-primary btn-panel navbar-btn glyphicon glyphicon-menu-right", id: "btnShow-Menu", "data-ref": "#left-panel", ref: "leftBtn", onClick: pageComponent.showPanel});
    }
    if (pageComponent && pageComponent.refs.slipPanel) {
      navBtnRight = React.createElement(NavButton, {className: "visible-xs-inline-block btn btn-primary btn-panel navbar-btn glyphicon glyphicon-menu-left navbar-btn-right", id: "btnShow-SlipList", "data-ref": "#right-panel", ref: "rightBtn", onClick: pageComponent.showPanel});
    }
    return (
      React.createElement("nav", {className: "navbar navbar-default navbar-fixed-top", id: "navbar"}, 
        React.createElement("div", {className: "row"}, 
          React.createElement("div", {className: "col-xs-2"}, 
            navBtnLeft
          ), 
          React.createElement("div", {className: "col-xs-8", id: "page-title"}, 
            React.createElement("h3", {className: "navbar-header text-center"}, this.props.title)
          ), 
          React.createElement("div", {className: "col-xs-2"}, 
            navBtnRight
          )
        )
      )
    );
  }
});
