'use strict'

var LoginForm = React.createClass({displayName: "LoginForm",
  getInitialState: function() {
    return ({});
  },
  
  handleFBLogin: function(e) {
    e.preventDefault();
    FBLogin();
  },
  
  handleEmailLogin: function(e) {
    e.preventDefault();
    logIntoApp({
      email: $('.form-signin input#email').val(),
      password: $('.form-signin input#password').val()
    });
  },
  
  render: function() {
    var alertMsg;
    if (this.props.error) {
      alertMsg = React.createElement("div", {className: "alert alert-danger alert-xs"}, this.props.error);
    }
    
    return (
      React.createElement("div", {className: "well well-login"}, 
        alertMsg, 
        React.createElement("button", {id: "fb-login", className: "btn btn-fb btn-block", onClick: this.handleFBLogin}, React.createElement("i", {className: "fa fa-facebook"}), " Login with Facebook"), 
        React.createElement("div", {className: "socialSplitter"}, 
          React.createElement("hr", null), 
          React.createElement("span", null, "OR sign in")
        ), 
        React.createElement("form", {className: "form-signin"}, 
          React.createElement("div", {className: "form-group"}, 
            React.createElement("label", {htmlFor: "email", className: "sr-only"}, "Email"), 
            React.createElement("input", {type: "email", className: "form-control", id: "email", required: true, placeholder: "Email"})
          ), 
          React.createElement("div", {className: "form-group"}, 
            React.createElement("label", {htmlFor: "password", className: "sr-only"}, "Password"), 
            React.createElement("input", {type: "password", className: "form-control", id: "password", required: true, placeholder: "Password"})
          ), 
          /*<div className="checkbox">
            <label>
              <input type="checkbox" /> Remember me
            </label>
          </div>*/
          React.createElement("button", {id: "submit", className: "btn btn-primary btn-block", onClick: this.handleEmailLogin}, "Sign in")
        )
      )
    );
  }
});