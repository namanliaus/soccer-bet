var SponsorButton = React.createClass({displayName: "SponsorButton",
  render: function() {
    return (
      React.createElement("button", {className: "btn btn-primary form-control", onClick: this.handlerSponsor}, "One click will add one time sponsoring")
    );
  }
});

function waitForSponsorListReady() {
  var sponsorList = pageComponent.refs.sponsorList;

  if (!sponsorList) {
    setTimeout(waitForSponsorListReady, 100);
    return;
  }

  sponsorList.setState({
    sponsorButton: {
      render: function() {
        return React.createElement(SponsorButton, null);
      }
    }
  });
}

waitForSponsorListReady();