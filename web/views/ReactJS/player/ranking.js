var RankingItem = React.createClass({displayName: "RankingItem",
  render: function() {
    return (
      React.createElement("tr", {ref: "rowElement"}, 
        React.createElement("td", {className: "text-center"}, this.props.index + 1), 
        React.createElement("td", {className: "text-center"}, this.props.data.userName), 
        React.createElement("td", {className: "text-center"}, roundFloat2dp(this.props.data.totalBet)), 
        React.createElement("td", {className: "text-center"}, roundFloat2dp(this.props.data.totalWin)), 
        React.createElement("td", {className: "text-center"}, roundFloat2dp(this.props.data.winRate))
      )
    );
  }
});

var RankingList = React.createClass({displayName: "RankingList",
  componentDidMount: function() {
    requestJSON({
      url: "player/ranking_data",
      method: "POST",
      success: function(json) {
        if (json.status == "ok") {
          this.setState({ranking: json.data});
        } else {
          showError(json);
        }
      }.bind(this)
    });
  },

  render: function() {

    var items = null;

    if (this.state && this.state.ranking) {
      items = this.state.ranking.map(function(item, idx) {
        return React.createElement(RankingItem, {key: item._id, index: idx, data: item})
      });
    }

    return (
      React.createElement("div", {className: "multiple-components"}, 
        React.createElement("table", {className: "table table-striped table-bordered"}, 
          React.createElement("thead", null, 
            React.createElement("tr", null, 
              React.createElement("th", {className: "text-center"}), 
              React.createElement("th", {className: "text-center"}, "Name"), 
              React.createElement("th", {className: "text-center"}, "Bet"), 
              React.createElement("th", {className: "text-center"}, "Won"), 
              React.createElement("th", {className: "text-center"}, "Win Rate")
            )
          ), 
          React.createElement("tbody", null, 
            items
          )
        )
      )
    );
  }
});

pageComponent.setState({
  title: "Ranking",
  pageContent: {
    render: function() {
      return React.createElement(RankingList, {ref: "rankingList"})
    }
  }
});