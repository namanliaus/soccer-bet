var SlipItem = React.createClass({displayName: "SlipItem",

  handleRemove: function() {
    var _data = {
      slip: JSON.stringify({
        matchId: this.props.data.matchId,
        marketId: this.props.data.marketId,
        oddId: this.props.data.oddId,
        slipId: this.props.data._id
      })
    };

    pageComponent.refs.slipPanel.sendRequestAndUpdate(_data);
  },

  handleChangeBetAmount: function(e) {

    var val = e.target.value;

    // the number input auto filters the strings so when user enters an invalid char
    // val is an empty string.
    if (val.length == 0) {
      this.setState({
        estimate: "",
        betAmount: ""
      });
      e.target.value = "";
      return;
    }

    // cannot be zero or negative
    if (parseFloat(val) <= 0.0) {
      this.setState({estimate: ""});
      return;
    }

    this.setState({
      betAmount: val,
      estimate: roundFloat2dp(this.props.data.oddRate * val)
    });
  },

  handleBet: function() {
    var _data = {
      slip: JSON.stringify({
        matchId: this.props.data.matchId,
        marketId: this.props.data.marketId,
        oddId: this.props.data.oddId,
        slipId: this.props.data._id,
        betAmount: this.state.betAmount
      })
    };

    pageComponent.refs.slipPanel.sendRequestAndUpdate(_data);
  },

  getInitialState: function() {
    return {
      betAmount: "",
      estimate: ""
    }
  },

  render: function() {
    return (
      React.createElement("div", {className: "list-group-item slip-item"}, 
        React.createElement("div", null, 
          React.createElement("div", {className: "slip-button-close"}, 
            React.createElement("button", {type: "button", className: "btn btn-sm btn-danger slip-button-close", onClick: this.handleRemove}, 
              React.createElement("span", {className: "glyphicon glyphicon-remove slip-button-close"})
            )
          ), 
          React.createElement("span", {className: "slip-match"}, this.props.data.matchName), 
          React.createElement("span", {className: "slip-match-time"}, " ", moment.unix(this.props.data.matchTime).local().format(DATETIME_FORMAT))
        ), 
        React.createElement("div", {className: "slip-market"}, this.props.data.marketName, " - ", this.props.data.oddName, " @", this.props.data.oddRate), 
        React.createElement("div", {className: "row"}, 
          React.createElement("div", {className: "col-xs-9 slip-bet-amount"}, 
            React.createElement("div", {className: "input-group"}, 
              React.createElement("span", {className: "input-group-addon slip-estimate-number"}, this.state.estimate), 
              React.createElement("input", {type: "number", className: "form-control input-sm", size: "6", min: "0", "place-holder": "Bet Amount", value: this.state.betAmount, onChange: this.handleChangeBetAmount})
            )
          ), 
          React.createElement("div", {className: "col-xs-3 slip-button-save"}, 
            React.createElement("button", {type: "button", className: "btn btn-sm btn-success form-control slip-button-save", onClick: this.handleBet, disabled: !this.state.betAmount || this.state.betAmount < 1}, 
              React.createElement("span", {className: "glyphicon glyphicon-ok"})
            )
          )
        )
      )
    )
  }
});