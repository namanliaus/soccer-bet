'use strict';

var ReactTransitionGroup = React.addons.TransitionGroup;

var SlipPanel = React.createClass({displayName: "SlipPanel",

  sendRequestAndUpdate: function(_data) {
    requestJSON({
      url: "player/slip_manipulate",
      method: "POST",
      data: _data,
      success: function(json) {
        if (json.status == "ok") {
          // update slip list
          this.onSlipListCome(json.data);
          // commenting for reducing data transfer
          // // tell matchList to update
          // pageComponent.refs.matchList.requestData();
        } else {
          showError(json);
        }
      }.bind(this)
    });
  },

  onSlipListCome: function(data) {
    var slips = data.slips;
    var userPoint = data.userPoint;

    var indexedSlips = {length: 0};
    for (var i = 0; i < slips.length; i++) {
      indexedSlips[slips[i].matchId + slips[i].marketId + slips[i].oddId] = i;
      indexedSlips.length++;
    }
    pageComponent.setState({
      slips: slips,
      indexedSlips: indexedSlips
    });
    this.refs.userPointComponent.setState({point: userPoint});
  },

  requestData: function() {
    // ask for a list of unsaved slips
    requestJSON({
      url: "player/slip_list",
      method: "POST",
      success: function(json) {
        if (json.status == "ok") {
          this.onSlipListCome(json.data);
        } else {
          showError(json);
        }
      }.bind(this)
    });
  },

  render: function() {
    var slipItems = [];
    if (this.props.slips) {

      this.props.slips.forEach(function(slip, idx) {
        if (slip.betAmount == 0) {
          slipItems.push(
            // add item add/remove effect to the slip list
            React.createElement(AnimationItem, {transitionName: "listitem-hide", key: "listitem-hide-"+slip._id, numTransIn: "1", numTransOut: "2"}, 
              React.createElement(SlipItem, {key: slip._id, data: slip, idx: idx})
            )
          );
        }
      });

      if (slipItems.length == 0) {
        // if there is no slip to show then replace the list with an alert
        slipItems = React.createElement("div", {className: "alert alert-info", role: "alert"}, "Unsaved slips of the future matches are listed here. You can select them from the list of matches then place the bets and save them.");
      }
    } else {
      this.requestData();
    }

    return (
      React.createElement("div", {className: "hidden-xs col-xs-3 col-md-2 list-group", id: "right-panel"}, 
        React.createElement("div", {className: "list-group-item active text-center", onClick: pageComponent.hidePanel.bind(null, "#right-panel")}, 
          React.createElement(UserPoint, {ref: "userPointComponent"})
        ), 
        React.createElement(ReactTransitionGroup, null, 
          slipItems
        )
      )
    );
  }
});

pageComponent.setState({
  slipPanel: {
    render: function() {
      return React.createElement(SlipPanel, {slips: pageComponent.state.slips, ref: "slipPanel"});
    }
  }
});