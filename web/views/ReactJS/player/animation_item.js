var AnimationItem = React.createClass({displayName: "AnimationItem",
  componentWillEnter: function(cb) {
    var $el = $(this.getDOMNode());
    $el.addClass(this.props.transitionName + "-enter");
    setTimeout(function(transitionName, el, numTrans, callback) {
      var count = numTrans;
      el.on("webkitTransitionEnd transitionend", function(e) {
        count--;
        if (count == 0) {
          this.off("webkitTransitionEnd transitionend");
          callback();
        }
      }.bind(el));
      el.addClass(transitionName + "-enter-active");
    }.bind(this, this.props.transitionName, $el, this.props.numTransIn, cb), 1000/60);
  },
  
  componentDidEnter: function() {
    var $el = $(this.getDOMNode());
    $el.removeClass(this.props.transitionName + "-enter");
    $el.removeClass(this.props.transitionName + "-enter-active");
  },
  
  componentWillLeave: function(cb) {
    var $el = $(this.getDOMNode());
    var count = this.props.numTransOut;
    $el.on("webkitTransitionEnd transitionend", function(e) {
      count--;
      if (count == 0) {
        this.off("webkitTransitionEnd transitionend");
        cb();
      }
    }.bind($el));
    
    $el.addClass(this.props.transitionName + "-leave");
    $el.css("margin-top", - Math.round($el.height()));
    $el.addClass(this.props.transitionName + "-leave-active");
  },
  
  componentDidLeave: function() {
    var $el = $(this.getDOMNode());
    $el.removeClass(this.props.transitionName + "-leave");
    $el.removeClass(this.props.transitionName + "-leave-active");
  },
  
  render: function() {
    return(
      React.createElement("div", {className: this.props.className, key: this.props.key}, 
        this.props.children
      )
    );
  }
});