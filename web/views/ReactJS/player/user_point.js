'use strict'

var UserPoint = React.createClass({displayName: "UserPoint",
  getUserPoint: function() {
    return this.state.point;
  },

  getInitialState: function() {
    return {point: 0};
  },

  requestData: function() {
    requestJSON({
      url: 'player/user_point',
      method: 'POST',
      success: function(json) {
        if (json.status == "ok") {
          this.setState({point: json.data.remainingPoint});
        } else {
          showError(json);
        }
      }.bind(this)
    });
  },

  render: function() {
    return (
      React.createElement("div", {className: "menu-username"}, "Points: ", this.state.point)
    );
  }
});