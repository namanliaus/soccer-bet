var MarketItem = React.createClass({displayName: "MarketItem",
  
  render: function() {

    var colCount = 0;
    if (this.props.market.odds.length <= 3) {
      colCount = this.props.market.odds.length;
    } else {
      if (this.props.market.odds.length % 3 == 0 || this.props.market.odds.length > 4) {
        colCount = 3;
      } else {
        colCount = 2;
      }
    }
    var gridSize = 12 / colCount;
    
    // initialize a list of rendering items arranged in columns
    var renderQueue = [];
    for (var i = 0; i < colCount; i++) {
      renderQueue.push([]);
    }

    // render odd items
    var currentColumn = 0;
    var maxRowCount = 0;

    this.props.market.odds.forEach(function(odd, idx) {
      
      if (odd.slips && odd.slips.length > 0) {
        // this is the case when player views history. Each odd contains list of slips.
        // We put them all in the same column

        for (var i = 0; i < odd.slips.length; i++) {
          var slip = odd.slips[i];
          // attach matchTime
          slip.matchTime = odd.matchTime;
          renderQueue[currentColumn].push(
            React.createElement(OddItem, {gridSize: gridSize, data: odd, key: slip._id, matchId: this.props.matchId, marketId: this.props.market._id, slip: slip})
          )
        }
      } else {
        var slipList = this.props.slipList
        var slip = null;
        if (slipList) {
          var slipIdx = slipList[
            this.props.matchId +
            this.props.market._id +
            odd._id
          ];
          // a mapped item exists
          if (slipIdx != null) {
            slip = pageComponent.state.slips[slipIdx];
          }
        }
        
        renderQueue[currentColumn].push(
          React.createElement(OddItem, {gridSize: gridSize, data: odd, key: odd._id, matchId: this.props.matchId, marketId: this.props.market._id, slip: slip})
        );
      }
      
      if (maxRowCount < renderQueue[currentColumn].length) {
        maxRowCount = renderQueue[currentColumn].length;
      }
      
      if (currentColumn == colCount - 1) {
        currentColumn = 0;
      } else {
        currentColumn++;
      }
    }.bind(this));

    // flatten the renderQueue
    var odds = [];
    for (var row = 0; row < maxRowCount; row++) {
      for (var col = 0; col < renderQueue.length; col++) {
        if (row < renderQueue[col].length) {
          // add a real slip
          odds.push(renderQueue[col][row]);
        } else {
          // add an empty cell just to fill the space
          odds.push(
            React.createElement("div", {className: "col-xs-" + this.props.gridSize + " odd-cell", key: col + "-" + row})
          )
        }
      }
    }
    

    // render the market;
    return (
      React.createElement("div", {className: "market-item"}, 
        React.createElement("div", {className: "market-head", onClick: this.handleClick}, this.props.market.name), 
        React.createElement("div", {className: "row row-odds"}, 
          odds
        )
      )
    );
  }
});