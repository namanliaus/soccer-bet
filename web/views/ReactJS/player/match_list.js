var MatchList = React.createClass({displayName: "MatchList",
  
  submitOddSelection: function(data) {
    pageComponent.refs.slipPanel.sendRequestAndUpdate(data);
  },
  
  handleOddClick: function(data) {
    // analyze data before submitting
    // if the odd is already bet by user then need to get confirmation before removing it
    // otherwise, just toggle plain/wait states
    var submitData = {
      slip: JSON.stringify({
        oddId: data.oddId,
        marketId: data.marketId,
        matchId: data.matchId,
        slipId: data.slipId
      })
    };
    
    // user has a slip with clicked odd?
    if (data.slip) {
      if (data.slip.betAmount > 0) {
        showModalDialog({
          title: "Removing bet",
          content: "You just clicked to remove a bet you have saved. Are you sure?",
          type: "danger",
          btn1: {
            text: "OK",
            onclick: function() {
              // change the odd style immediately
              this.submitOddSelection(submitData);
            }.bind(this)
          },
          btn2: {text: "Cancel"}
        });
        
        return;
      }
    }
    
    this.submitOddSelection(submitData);
  },
  
  requestData: function(filter) {

    requestJSON({
      url: this.props.dataURL,
      method: "POST",
      data: {filter: JSON.stringify(filter)},
      success: function(json) {
        if (json.status == "ok") {
          // console.log(json);
          pageComponent.setState({
            matches: json.data.matches,
            timeFilter: json.data.filter
          })
        } else {
          showError(json);
        }
      }
    })
  },

  render: function() {
    
    var matches = this.props.matches.map(function (match) {
      return (
        React.createElement(MatchItem, {match: match, key: match._id, slipList: this.props.slips})
      )
    }.bind(this));
      
    if (matches.length == 0) {
      // if there is no match in the list to show replace the list with an alert
      matches = React.createElement("div", {className: "alert alert-warning", role: "alert"}, "Sorry. There is no data found within the selected time range. Please come back later or broaden your filter.")
    }
    
    return (
      React.createElement("div", {className: "matches"}, 
        React.createElement(TimeFilterPanel, {parent: this, past: this.props.timeFilter.showPast, future: this.props.timeFilter.showFuture, ref: "timeFilterPanel"}), 
        matches
      )
    )
  }
});