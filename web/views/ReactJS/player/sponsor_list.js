var SponsorItem = React.createClass({displayName: "SponsorItem",
  render: function() {
    return (
      React.createElement("tr", {ref: "rowElement"}, 
        React.createElement("td", {className: "text-center"}, this.props.index + 1), 
        React.createElement("td", {className: "text-center"}, this.props.sponsor.userName), 
        React.createElement("td", {className: "text-center"}, "x", this.props.sponsor.sponsor)
      )
    );
  }
});

var SponsorList = React.createClass({displayName: "SponsorList",
  getInitialState: function() {
    return {};
  },

  handlerSponsor: function() {
    showModalDialog({
      type: "info",
      title: "Please confirm",
      content: "Thank you for your sponsorship. Once you submit, you won't be able to revoke it.",
      btn1: {
        text: "OK",
        onclick: function() {
          requestJSON({
            url: "player/sponsor_add",
            method: "POST",
            success: function(json) {
              if (json.status == "ok") {
                pageComponent.setState({sponsorList: json.data});
              } else {
                showError(json);
              }
            }
          });
        }
      },
      btn2: {text: "Cancel"}
    });
  },

  render: function() {
    var sponsors = this.props.sponsors.map(function(sponsor, idx) {
      return React.createElement(SponsorItem, {key: sponsor._id, index: idx, sponsor: sponsor})
    });

    var button = null;
    if (this.state.sponsorButton) {
      button = this.state.sponsorButton.render();
    }

    return (
      React.createElement("div", {className: "multiple-components"}, 
        React.createElement("table", {className: "table table-striped table-bordered"}, 
          React.createElement("thead", null, 
            React.createElement("tr", null, 
              React.createElement("th", {className: "text-center"}), 
              React.createElement("th", {className: "text-center"}, "Name"), 
              React.createElement("th", {className: "text-center"}, "Times")
            )
          ), 
          React.createElement("tbody", null, 
            sponsors
          )
        ), 
        button
      )
    );
  }
});

requestJSON({
  url: "player/sponsor_list_data",
  method: "POST",
  success: function(json) {
    if (json.status == "ok") {
      pageComponent.setState({
        title: "Sponsors",
        sponsorList: json.data,
        pageContent: {
          render: function() {
            return React.createElement(SponsorList, {sponsors: pageComponent.state.sponsorList, ref: "sponsorList"})
          }
        }
      });
    } else {
      showError(json);
    }
  }
})