var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    OddSchema = require("./odd").Schema;

// modal for markets subdocument of Match
var MarketSchema = Schema({
  colCount: {type: Number},
  name: {type: String},
  odds: [OddSchema],
  oddCount: {type: Number, default: 0}
});

MarketSchema.pre("save", function(next) {
  // update subdocument (odds) count
  this.oddCount = this.odds.length;
  next();
});

var MarketModel = {
  Schema: MarketSchema
};

module.exports = MarketModel;