var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    MarketSchema = require("./market").Schema,
    userModel = require("./user"),
    userPointModel = require("./userpoint");

var moment = require("moment");
var alert = require("../libs/alert_json");

var MatchSchema = Schema({
  name: {type: String},
  time: {type: Number},
  result: {type: String},
  markets: [MarketSchema],
  marketCount: {type: Number, default: 0}
});
MatchSchema.index({name: 1, time: 1}, {unique: true});
// the below indexes do not seem to work
// MatchSchema.index({"markets._id": 1});
// MatchSchema.index({"markets.odds._id": 1});
// MatchSchema.index({"markets.odds.slips._id": 1});

MatchSchema.pre("save", function(next) {
  this.marketCount = this.markets.length;
  next();
});

// @LESSON: multi level document sorting seems not work.
// Have to use aggregation with unwinding.
// Because of unwinding, we then have to group and project the result.
// There must be a more efficient way to do this.

// read time filter and apply a new limit when need
MatchSchema.statics.verifyTimeFilter = function(filterParams) {
  var nowUNIX = moment().utc().unix();
  var result = {
    // default values are current time
    start_time: nowUNIX,
    end_time: nowUNIX
  };

  if (!filterParams) {
    filterParams = {
      // allow results in the future but not the past
      future: true,
      past: false
    };
  }

  // if there is no filter provided from Front-End
  if (!filterParams.filter) {

    if (filterParams.longFuture) {
      // end time is in next 30 days
      result.end_time = moment().add(30, "d").utc().unix();
    } else if (filterParams.future) {
      // add tomorrow to filter
      result.end_time = moment().add(1, "d").utc().unix();
    }

    if (filterParams.longPast) {
      // start time was in last 30 days
      result.start_time = moment().subtract(30, "d").utc().unix();
    } else if (filterParams.past) {
      // add yesterday to filter
      result.start_time = moment().subtract(1, "d").utc().unix();
    }
  } else {
    result = JSON.parse(filterParams.filter);

    // the filter from client must follow the restriction in each function
    if (!filterParams.past) {
      if (result.start_time < nowUNIX) {
        result.start_time = nowUNIX;
      }
    }
    if (!filterParams.future) {
      if (result.end_time > nowUNIX) {
        result.end_time = nowUNIX;
      }
    }
  }

  return result;
}

// return list of matches scheduled in a time range
MatchSchema.statics.getMatchesByTime = function(options, callback) {
  var filter = this.verifyTimeFilter(options.timeFilter);

  this
  .where("time").gte(filter.start_time).lte(filter.end_time)
  .where("marketCount").gt(0)
  // get every thing but the slips
  .select("-markets.odds.slips")
  // older matches first
  .sort("time")
  .exec(function(err, matches) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }
    if (matches === null) {
      matches = [];
    }

    // format the page to show
    callback(null, alert.message(alert.CONST.INFO_JSON_RETURNED, {matches: matches, filter: filter}));
  });
}

// return list of matches, markets, and odds which a user has selected or bet
MatchSchema.statics.getMatchesWithSlips = function(options, callback) {
  var filter = this.verifyTimeFilter(options.timeFilter);
  var userId = options.userId;

  // @LESSON: Mongoose aggregation does not automatically convert id to ObjectId

  this
  .aggregate([
    {$match: {
      time: {$gte: filter.start_time, $lte: filter.end_time}
    }},
    // unwind step-by-step to slips
    {$unwind: "$markets"},
    {$unwind: "$markets.odds"},
    {$unwind: "$markets.odds.slips"},
    // find all slips added by a user
    {$match: {"markets.odds.slips.userId": mongoose.Types.ObjectId(userId)}},
    // we have unwound 3 levels, now wind back by grouping and pushing data to array
    // since each user has one slip for one odd, we can merge slip and odd in the same level
    {$group: {
      _id: {
        matchId: "$_id",
        matchName: "$name",
        matchTime: "$time",
        marketId: "$markets._id",
        marketName: "$markets.name",
      },
      odds: {
        $push: {
          _id: "$markets.odds._id",
          name: "$markets.odds.name",
          rate: "$markets.odds.rate",
          slipId: "$markets.odds.slips._id",
          betAmount: "$markets.odds.slips.betAmount",
          won: "$markets.odds.slips.won",
          matchTime: "$time"
        }
      }
    }},
    {$sort: {"_id.marketId": -1}},
    // continue to wind
    {$group: {
      _id: {
        matchId: "$_id.matchId",
        matchName: "$_id.matchName",
        matchTime: "$_id.matchTime"
      },
      markets: {
        $push: {
          _id: "$_id.marketId",
          name: "$_id.marketName",
          odds: "$odds"
        }
      }
    }},
    // finally, massage the result
    {$project: {
      _id: "$_id.matchId",
      name: "$_id.matchName",
      time: "$_id.matchTime",
      markets: "$markets"
    }},
    {$sort: {"time": -1}}
  ])
  .exec(function(err, matches) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }
    if (matches === null) {
      matches = [];
    }

    callback(null, alert.message(alert.CONST.INFO_JSON_RETURNED, {matches: matches, filter: filter}));
  });
}

// add/change/delete a slip of a user
MatchSchema.statics.manipulateSlip = function(options, callback) {
  var now = moment().utc().unix();
  var _slip = options._slip;
  var userId = options.userId;

  // traverse down to the sub document containing the slip; starting from a match
  this.findById(_slip.matchId, function(err, match) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }
    if (match == null) {
      // the match was removed somehow
      callback(null, alert.message(alert.CONST.ERRO_MATCH_REMOVED));
      return;
    }

    // player cannot modify an old slip
    if (now > match.time) {
      callback(null, alert.message(alert.CONST.ERRO_MATCH_BET_CLOSED));
      return;
    }

    // look for the market that the slip sticks to
    var market = match.markets.id(_slip.marketId);
    if (market == null) {
      // the market was removed somehow
      callback(null, alert.message(alert.CONST.ERRO_MARKET_REMOVED));
      return;
    }
    // and then the odd
    var odd = market.odds.id(_slip.oddId);
    if (odd == null) {
      // the odd was removed somehow
      callback(null, alert.message(alert.CONST.ERRO_ODD_REMOVED));
      return;
    }

    var userPointToUpdate = 0;
    // auto detect action by checking data provided in slip
    // slip has no ID, it should be a new one added by user
    if (!_slip.slipId) {
      // check if user already selected this odd
      var selected = odd.slips && !odd.slips.every(function(slip) {
        if (slip.userId == userId) {
          // found user, break the loop
          return false;
        } else {
          return true;
        }
      });

      if (selected) {
        // inform user the problem that the slip exists
        // this may happen if 2 or more users log in with the same account at the same time
        // one might have added the slip, and this user did it again
        // OR, the GUI does not reflect the current status correctly
        callback(null, alert.message(alert.CONST.ERRO_SLIP_ADDED));
        return;
      }

      // push the slip to the existing slip list
      odd.slips.push({userId: userId});
    } else {
      // this is a recorded slip, user might have placed a bet or removed the slip
      var slip = odd.slips.id(_slip.slipId);

      if (slip != null) {
        // no bet amount was sent, should be a removal action
        if (!_slip.betAmount) {
          userPointToUpdate = slip.betAmount;
          slip.remove();
        } else {
          // a bet action
          slip.betAmount = _slip.betAmount;
          userPointToUpdate = -slip.betAmount;
        }
      }
    }

    // commit the slip change from match level
    match.save(function(err) {
      if (err) {
        callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
        return;
      }

      // if a slip is bet or remove, we may have to add/subtract user's remaining point
      if (userPointToUpdate !== 0) {
        console.log(userPointToUpdate);
        userModel.updateUserRemainingPoint({
          userId: userId,
          data: userPointToUpdate
        }, function(err, result) {
          console.log(result);
          if (err) {
            callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
            return;
          }

          // send back the a slip list
          MatchModel.getUserSlips({userId: userId}, callback);
        });
      } else {
        // send back the a slip list
        MatchModel.getUserSlips({userId: userId}, callback);
      }
    });
  });
};

// get all unsaved slips (of the matches in the future) of a user
MatchSchema.statics.getUserSlips = function(options, callback) {

  var filter = this.verifyTimeFilter(options.timeFilter);
  var userId = options.userId;

  // @LESSON: aggregation does not automatically convert id to ObjectId
  // get IDs of the slips that user selected but has not bet
  this
  .aggregate([
      {$match: {"time": {$gte: filter.start_time}}},
      // unwind step-by-step to slips
      {$unwind: "$markets"},
      {$unwind: "$markets.odds"},
      {$unwind: "$markets.odds.slips"},
      // find all slips added by one or all users
      {$match: {"markets.odds.slips.userId": mongoose.Types.ObjectId(userId)}},
      // flatten the result
      {$project: {
        _id: "$markets.odds.slips._id",
        matchId: "$_id",
        matchName: "$name",
        matchTime: "$time",
        marketId: "$markets._id",
        marketName: "$markets.name",
        oddId: "$markets.odds._id",
        oddName: "$markets.odds.name",
        oddRate: "$markets.odds.rate",
        betAmount: "$markets.odds.slips.betAmount",
        won: "$markets.odds.slips.won"
      }}
    ])
  .exec(function(err, slips) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }
    if (slips == null) {
      slips = [];
    }

    userModel.getUserRemainingPoint(options, function(err, user) {
      if (err) {
        callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
        return;
      }

      callback(null, alert.message(alert.CONST.INFO_JSON_RETURNED, {slips: slips, userPoint: user.remainingPoint}));
    });
  });
}

// History of betting:
// get all slips of all users (with user name) within a time range
// this is expensive because we try to look for user name from userId
// in each slip. Only use it with a group of few people or
// change the SlipSchema to embed user name in each slip
MatchSchema.statics.getSlipHistory = function(options, callback) {
  var filter = this.verifyTimeFilter(options.timeFilter);

  // find all matches, markets, odds, and slips with time filter
  this
  .aggregate([
    // time matching
    {$match: {
      time: {$gte: filter.start_time, $lte: filter.end_time}
    }},
    // unwind to odds to select only odds with slips
    {$unwind: "$markets"},
    {$unwind: "$markets.odds"},
    // only odds with slips
    {$match: {
      "markets.odds.slipCount": {$gt: 0}}
    },
    // since we unwind to the level of odds, we need to get back to the top level by grouping
    {$group: {
      _id: {
        match: {matchId: "$_id", matchName: "$name", matchTime: "$time"},
        market: {marketId: "$markets._id", marketName: "$markets.name"}
      },
      odds: {
        $push: {
          _id: "$markets.odds._id",
          name: "$markets.odds.name",
          rate: "$markets.odds.rate",
          slips: "$markets.odds.slips",
          matchTime: "$time"
        }
      }
    }},
    {$sort: {"_id.match.matchTime": -1, "_id.market.marketId": -1}},
    {$group: {
      _id: {
        match: {matchId: "$_id.match.matchId", matchName: "$_id.match.matchName", matchTime: "$_id.match.matchTime"}
      },
      markets: {
        $push: {
          _id: "$_id.market.marketId",
          name: "$_id.market.marketName",
          odds: "$odds"
        }
      }
    }},
    {$project: {
      _id: "$_id.match.matchId",
      name: "$_id.match.matchName",
      time: "$_id.match.matchTime",
      markets: "$markets"
    }}
  ])
  .exec(function(err, matches) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }
    if (matches == null) {
      matches = [];
    }

    // use built-in mongoose feature to get user name.
    // The result, however, is not pretty because each name becomes a child of each userId node.
    userModel.populate(
      matches,
      {path: "markets.odds.slips.userId", select: "userName"},
      function (err, updatedMatches) {
        if (err) {
          callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
          return;
        }
        callback(null, alert.message(alert.CONST.INFO_JSON_RETURNED, {matches: updatedMatches, filter: filter}));
      }
    )
  })
}

// save list of markets of a match
MatchSchema.statics.saveMarkets = function(options, callback) {
  // find the match
  this.findById(options.matchId, function(err, match) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }
    if (match == null) {
      callback(err, alert.message(alert.CONST.ERRO_NO_MATCH, err));
      return;
    }

    // set the market list
    match.markets = options.markets;
    // and save the match
    match.save(function(err, match, numberAffected) {
      if (err) {
        callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
        return;
      }

      // return list of matches
      MatchModel.getOpMatchList(options, callback);
    });
  });
}

// return a match list to operator or admin to manage
MatchSchema.statics.getOpMatchList = function(options, callback) {
  var filter = this.verifyTimeFilter(options.timeFilter, false);

  this
  .where("time")
  .gte(filter.start_time).lte(filter.end_time)
  .select("-markets -__v")
  .sort("-time")
  .exec(function(err, matches) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }
    if (matches === null) {
      matches = [];
    }

    // format the page to show
    callback(null, alert.message(alert.CONST.INFO_JSON_RETURNED, {matches: matches, filter: filter}));
  });
}

// add/update a match
MatchSchema.statics.saveMatch = function(options, callback) {

  if (!options.match._id) {
    options.match._id = new mongoose.Types.ObjectId;
  }

  this
  .where({_id: options.match._id})
  .setOptions({upsert: true})
  .update(options.match, function(err, result) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }

    // return all matches
    MatchModel.getOpMatchList(options, callback);
  });
}

// return details of a match for operator or admin to update its result
MatchSchema.statics.getOneMatchForUpdatingResult = function(options, callback) {
  this
  // find a match by its ID
  .findOne({_id: options.matchId})
  // exclude the slip list
  .select("-markets.odds.slips -v")
  .exec(function(err, match) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }
    if (match == null) {
      // what? someone else removed the match!
      callback(null, alert.message(alert.CONST.ERRO_MARKET_REMOVED));
      return;
    }

    callback(null, alert.message(alert.CONST.INFO_JSON_RETURNED, {match: match}));
  });
}

// update result of a match including score and odds selected as winning
MatchSchema.statics.updateMatchResult = function(options, callback) {
  // look for the match
  this.findById(options.matchId, function(err, match) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }

    if (match == null) {
      callback(null, alert.message(alert.CONST.ERRO_MARKET_REMOVED));
      return;
    }

    match.result = options.result;

    var wonOdds = JSON.parse(options.wonOdds);

    // list of users won the bets of this match
    var users = {length: 0};

    // mark odds as winning and calculate user points
    for (var oddId in wonOdds) {
      var market = match.markets.id(wonOdds[oddId].marketId);
      if (!market) {
        continue;
      }

      var odd = market.odds.id(oddId);
      if (!odd) {
        continue;
      }

      odd.isWinner = wonOdds[oddId].isWinner;
      // calculate user winning points
      for (var j = 0; j < odd.slips.length; j++) {
        var slip = odd.slips[j];

        if (!odd.isWinner) {
          slip.won = 0;
        } else {
          slip.won = slip.betAmount * (odd.rate - 1);
        }

        // points adding for each user
        if (users[slip.userId] != undefined) {
          users[slip.userId].bet += slip.betAmount;
          users[slip.userId].won += slip.won;
        } else {
          users[slip.userId] = {bet: slip.betAmount, won: slip.won};
          users.length += 1;
        }
      }
    }

    // save the changes to the match
    match.save(function(err) {
      if (err) {
        callback(err, alert.message(alert.CONST.ERRO_MARKET_REMOVED));
        return;
      }

      // no need to update user points if no one won the bets
      if (users.length == 0) {
        callback(null, alert.message(alert.CONST.INFO_DATA_UPDATED));
        return;
      }

      // save the points that users won in the match
      userPointModel.savePointsForMatch({
        matchId: match._id,
        users: users
      }, function(err, msg) {
        if (err) {
          callback(err, msg);
          return;
        }

        // sum all bet and won points for updating user win rate
        userPointModel
        .aggregate([
          {$group: {
            _id: {
              userId: "$userId"
            },
            totalBet: {$sum: "$bet"},
            totalWin: {$sum: "$won"}
          }}
        ])
        .exec(function(err, result) {
          if (err) {
            callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
            return;
          }

          userModel.updateUserWinRate({users: result}, callback);
        });

        callback(null, msg);
      });
    });
  });
}

var MatchModel = mongoose.model('Match', MatchSchema);

module.exports = MatchModel;