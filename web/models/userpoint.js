var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var alert = require('../libs/alert_json');

var UserPointSchema = Schema({
  userId: {type: ObjectId},
  matchId: {type: ObjectId},
  bet: {type: Number},
  won: {type: Number}
});
UserPointSchema.index({userId: 1, matchId: 1}, {unique: true});

UserPointSchema.statics.savePointsForMatch = function(options, callback) {
  var matchId = options.matchId;
  var users = options.users;
  var numRemainUser = users.length;
  var hasError = false;
  
  for (var userId in users) {
    if (userId == "length") continue;
    
    // find each of user and update points
    // only call callback when all users are processed
    this
    .findOne({
      matchId: matchId,
      userId: userId
    }, function(userId, err, model) {
      if (err) {
        hasError = true;
        numRemainUser = userProcessed(numRemainUser, hasError, callback);
        return;
      }
      
      var rate = 0;
      if (users[userId].bet > 0) {
        rate = users[userId].won / users[userId].bet;
      }
      
      // if there is no matched item, create a new one
      if (!model) {
        model = new UserPointModel({
          userId: userId,
          matchId: matchId,
          bet: users[userId].bet,
          won: users[userId].won,
          rate: rate
        });
      } else {
        model.bet = users[userId].bet;
        model.won = users[userId].won;
        model.rate = rate;
      }
      
      model.save(function(err) {
        if (err) {
          hasError = true;
        }
        
        numRemainUser = userProcessed(numRemainUser, hasError, callback);
      });
      // pass userId to findOne callback otherwise it can be any id from the for loop.
    }.bind(null, userId));
  }
}

function userProcessed(numRemainUser, hasError, callback) {
  numRemainUser--;
  // time to call the callback
  if (numRemainUser == 0) {
    // check if there is any error
    if (hasError) {
      callback(null, alert.message(alert.CONST.ERRO_DB_ACTION));
    } else {
      callback(null, alert.message(alert.CONST.INFO_DATA_UPDATED));
    }
  }
  
  return numRemainUser;
}

var UserPointModel = mongoose.model('UserPoint', UserPointSchema);

module.exports = UserPointModel;