var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
    
var SlipSchema = Schema({
  userId: {type: ObjectId, ref: "User"},
  betAmount: {type: Number, default: 0},
  won: {type: Number, default: 0}
});

var SlipModel = {
  Schema: SlipSchema,
}

module.exports = SlipModel;