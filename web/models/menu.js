module.exports = {
  formatMenuData: function(data, callback) {
    var ret = [];

    // for showing the menus of each user type clearly
    // the code is repeated instead of being optimized
    if (data.userType === 1) {
      // admin's menu
      ret = [
        {type: "header", username: data.userName},
        {type: "link", action: "player/sponsor_list", text: "Sponsors"},
        {type: "divider"},
        {type: "link", action: "admin/user_list", text: "Manage Users"},
        {type: "link", action: "admin/match_list", text: "Manage Matches"},
        {type: "divider"},
        {type: "link", action: "player/ranking", text: "Ranking"},
        {type: "link", action: "player/history", text: "History"},
        {type: "divider"},
        {type: "link", action: "player/settings", text: "Settings"},
        {type: "link", action: "logout", text: "Logout", style: "logout"}
      ];
    } else if (data.userType === 2) {
      // operator's menu
      ret = [
        {type: "header", username: data.userName},
        {type: "link", action: "player/sponsor_list", text: "Sponsors"},
        {type: "divider"},
        {type: "link", action: "player/match_list", text: "Matches"},
        {type: "link", action: "player/my_slip_list", text: "My Slips"},
        {type: "divider"},
        {type: "link", action: "op/match_list", text: "Update Results"},
        {type: "divider"},
        {type: "link", action: "player/ranking", text: "Ranking"},
        {type: "link", action: "player/history", text: "History"},
        {type: "divider"},
        {type: "link", action: "player/settings", text: "Settings"},
        {type: "link", action: "logout", text: "Logout", style: "logout"}
      ];
    } else if (data.userType === 3) {
      // normal player's menu
      ret = [
        {type: "header", username: data.userName},
        {type: "link", action: "player/sponsor_list", text: "Sponsors"},
        {type: "divider"},
        {type: "link", action: "player/match_list", text: "Matches"},
        {type: "link", action: "player/my_slip_list", text: "My Slips"},
        {type: "divider"},
        {type: "link", action: "player/ranking", text: "Ranking"},
        {type: "link", action: "player/history", text: "History"},
        {type: "divider"},
        {type: "link", action: "player/settings", text: "Settings"},
        {type: "link", action: "logout", text: "Logout", style: "logout"}
      ];
    }

    callback(ret);
  }
}