// this file contains commands for manipulating database at the development time
var util = require("util");
var mongoose = require("mongoose");
var moment = require("moment");

function inspect(obj) {
  console.log(util.inspect(obj, {showHidden: true, depth: null, colors: true}));
}

// clear the slips of a market of a user
function clearSlips() {
  var matchModel = require("./match");

  matchModel.findById("559ded25489ca4e00ce1bf62", function(err, match) {
    var odds = match.markets.id("55a4d1f9460469fc0c010231").odds;
    console.log(odds);
    return;

    odds.forEach(function(odd) {
      odd.slips.forEach(function(slip) {
        slip.remove();
      });
    });

    match.save(function(err, match, number) {
      console.log("match.save", err);
    });
  });
}

function getMatchesWithSlips() {
  var matchModel = require("./match");
  matchModel.getMatchesWithSlips({}, function(err, msg) {
    console.log(JSON.stringify(msg));
  });
}

function userSlips() {
  var matchModel = require("./match");
  matchModel.getUserSlips({userId: "559ba9ef66da887c148b08a2"}, function(err, msg) {
    console.log(JSON.stringify(msg));
  });
}

function history() {
  var matchModel = require("./match");
  matchModel.getSlipHistory({futureOnly: false}, function(err, msg) {
    console.log(JSON.stringify(msg));
  });
}

function testTimeFilter() {
  var matchModel = require("./match");
  var moment = require("moment");
  console.log(moment().utc().unix());

  console.log(matchModel.verifyTimeFilter({
    //filter: JSON.stringify({start_time: -1, end_time: moment().add(10, "d").utc().unix()}),
    past: true,
    future: true
  }));
}

function getOneMatch() {
  var matchModel = require("./match");
  matchModel.getOneMatchForUpdatingResult({matchId: "55af2dfe24b012c429f18965"}, function(err, msg) {
    console.log(JSON.stringify(msg));
  });
}

function clearWinOdds() {
  var matchModel = require("./match");
  matchModel.findById("55af2dfe24b012c429f18965", function(err, match) {
    for (var i = 0; i < match.markets.length; i++) {
      var market = match.markets[i];
      for (var j = 0; j < market.odds.length; j++) {
        market.odds[j].isWinner = false;
      }
    }

    match.save(function(e) {
      console.log(e);
    });
  });
}

function userWinRate() {
  var userPointModel = require("./userpoint");
  userPointModel
  .aggregate([
    {$group: {
      _id: {
        userId: "$userId"
      },
      totalBet: {$sum: "$bet"},
      totalWin: {$sum: "$won"}
    }}
  ])
  .exec(function(err, result) {
    console.log(result);
  });
}

function sponsor() {
  var userModel = require("./user");
  userModel.sponsor({userId: "559ba9ef66da887c148b08a2"}, function(err, msg) {
    console.log(JSON.stringify(msg));
  });
}

function remainPoint() {
  var userModel = require("./user");
  userModel.getUserRemainingPoint({userId: "559ba9ef66da887c148b08a2"}, function(err, msg) {
    console.log(JSON.stringify(msg));
  });
}

function createAdminUser() {
  var userModel = require("./user");
  var admin = new userModel({
    userName: "Admin",
    password: "admin123",
    email: "nam.anliaus@gmail.com",
    userType: 1,
    initPoint: 0,
    sponsor: 0,
    remainingPoint: 0,
    totalBet: 0,
    totalWin: 0,
    winRate: 0
  });

  admin.save(function(err) {
    console.log(err);
  });
}

module.exports = createAdminUser