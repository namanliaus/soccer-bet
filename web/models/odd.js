var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    SlipSchema = require("./slip").Schema;
    
var OddSchema = Schema({
  name: {type: String, require: true},
  rate: {type: Number, require: true},
  slipCount: {type: Number},
  slips: [SlipSchema],
  isWinner: {type: Boolean, default: false}
});

OddSchema.pre("save", function(next) {
  this.slipCount = this.slips.length;
  next();
})

var OddModel = {
  Schema: OddSchema
};

module.exports = OddModel;