var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs'),
    SALT_WORK_FACTOR = 10;

var alert = require('../libs/alert_json');

var UserSchema = Schema({
  fbId: {type: String},
  userName: {type: String},
  password: {type: String},
  email: {type: String, required: true, unique: true},
  userType: {type: Number},
  initPoint: {type: Number, default: 200},
  sponsor: {type: Number, default: 0},
  remainingPoint: {type: Number, default: 200},
  totalBet: {type: Number, default: 0},
  totalWin: {type: Number, default: 0},
  winRate: {type: Number, default: 0}
});

UserSchema.pre('save', function(next) {

  if (this.isModified("email")) {
    // make sure the email is in lower case
    this.email = this.email.toLowerCase();
  }

  if (this.isModified("password")) {
    // encrypt the password
    this.setPassword(this.password);
  }

  next();
});

UserSchema.methods.setPassword = function(password) {
  this.password = bcrypt.hashSync(password, bcrypt.genSaltSync(SALT_WORK_FACTOR));
};

UserSchema.methods.matchPassword = function(candidatePassword, cb) {
  return bcrypt.compareSync(candidatePassword, this.password);
};

UserSchema.statics.changeUserPassword = function(options, callback) {
  this.findById(options.userId, function(err, user) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }

    if (user == null) {
      callback(null, alert.message(alert.CONST.ERRO_NO_USER));
      return;
    }

    if (!user.matchPassword(options.oldPassword)) {
      callback(alert.CONST.ERRO_WRONG_PWD, alert.message(alert.CONST.ERRO_WRONG_PWD));
      return;
    }

    user.password = options.newPassword;
    user.save(function(err) {
      if (err) {
        callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
        return;
      }

      callback(null, alert.message(alert.CONST.INFO_DATA_UPDATED));
    });
  });
};

UserSchema.statics.userLogin = function(options, callback) {
  this
  .findOne(
    {email: options.email.toLowerCase()},
    function(err, user) {
    if (err) {
      callback(alert.CONST.ERRO_CHECK_USR_ERR, alert.message(alert.CONST.ERRO_CHECK_USR_ERR));
      return;
    }
    // no user found (the email is not in database)
    if (user === null) {
      // check if user is signing in as a facebook user
      if (options.fbId !== null) {
        // save this user as unregistered so that admin can approve it later
        var newUser = new UserModel({
          fbId: options.fbId,
          email: options.email,
          userName: options.username,
          userType: 4,
          initPoint: 0
        });
        newUser.save();
        // save user but still inform that the FB email cannot be used to sign in now
        // admin will have to review and change user type later.
        callback(alert.CONST.ERRO_UNREGISTERED_FBUSER, alert.message(alert.CONST.ERRO_UNREGISTERED_FBUSER));
      } else {
        // no email match, inform that
        callback(alert.CONST.ERRO_UNREGISTERED_USER, alert.message(alert.CONST.ERRO_UNREGISTERED_USER));
      }
      // unsuccessful loggin, return here
      return;
    }

    // found one, check its FacebookID or password
    if (options.fbId !== null) {
      // if user tried to login with FB but has not been approved
      if (user.userType > 3) {
        callback(alert.CONST.ERRO_UNREGISTERED_FBUSER, alert.message(alert.CONST.ERRO_UNREGISTERED_FBUSER));
        return;
      }
    } else if (options.password !== null) {
      // check user password
      if (!user.matchPassword(options.password)) {
        callback(alert.CONST.ERRO_WRONG_PWD, alert.message(alert.CONST.ERRO_WRONG_PWD));
        return;
      }
    }

    // return user data
    callback(null, user);
  });
};

UserSchema.statics.getUserList = function(callback) {
  this.find(function(err, users) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }
    if (users === null) {
      callback(err, alert.message(alert.CONST.ERRO_NO_USER));
      return;
    }

    // format the page to show
    callback(null, alert.message(alert.CONST.INFO_JSON_RETURNED, {users: users}));
  });
};

UserSchema.statics.saveUser = function(options, callback) {
  var user = options.user;

  this
  .findById(
    user._id,
    function(err, model) {
      if (err) {
        callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
        return;
      }

      if (model == null) {
        model = new UserModel(user);
      } else {
        // update the doc
        model.email = user.email;
        model.userName = user.userName;
        model.password = user.password;
        model.initPoint = user.initPoint;
        model.userType = user.userType;
      }

      // use this save method to take a chance to encrypt password
      model.save(function(err, newDoc) {
        if (err) {
          callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
          return;
        }

        // return all users
        UserModel.getUserList(callback);
      });
    }
  );
};

UserSchema.statics.deleteUser = function(options, callback) {
  this
  .findOneAndRemove(
    {_id: options.userId},
    function(err, user) {
      if (err) {
        callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
        return;
      }
      // return all users
      UserModel.getUserList(callback);
    }
  );
};

UserSchema.statics.updateUserWinRate = function(options, callback) {
  // loop through list of users and update each user asynchronously
  for (var i = 0; i < options.users.length; i++) {
    var up = options.users[i];
    // bind the data to the function to make sure that it picks up correct data
    this.findById(up._id.userId, function(up, err, user) {
      // store and calculate all factors for later quick reference
      user.totalBet = up.totalBet;
      user.totalWin = up.totalWin;
      if (up.totalBet > 0) {
        user.winRate = up.totalWin / up.totalBet;
      } else {
        user.winRate = 0;
      }

      user.remainingPoint = user.initPoint + user.sponsor * user.initPoint + up.totalWin - up.totalBet;

      // finally, save this user
      user.save();

    }.bind(null, up));
  }
};

UserSchema.statics.getSponsorList = function(callback) {
  // exclude administrator in the search
  this
  .find({userType: {$gt: 1}})
  .select("userName sponsor")
  .sort("-sponsor")
  .exec(function(err, users) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }

    if (users == null) {
      users = [];
    }

    callback(null, alert.message(alert.CONST.INFO_JSON_RETURNED, users));
  });
};

UserSchema.statics.sponsor = function(options, callback) {
  this.findById(options.userId, function(err, user) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }

    user.sponsor += 1;
    user.remainingPoint += user.initPoint;

    user.save(function(err) {
      if (err) {
        callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
        return;
      }

      UserModel.getSponsorList(callback);
    });
  });
};

UserSchema.statics.getRankingList = function(callback) {
  this
  .find({userType: {$gt: 1}})
  .select("userName totalBet totalWin winRate")
  .sort("-totalWin")
  .exec(function(err, users) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }

    if (users == null) {
      users = [];
    }

    callback(null, alert.message(alert.CONST.INFO_JSON_RETURNED, users));
  });
};

UserSchema.statics.getUserRemainingPoint = function(options, callback) {
  this
  .findOne({_id: options.userId})
  .select("remainingPoint -_id")
  .exec(function(err, user) {
    if (err) {
      callback(err, alert.message(alert.CONST.ERRO_DB_ACTION, err));
      return;
    }

    if (options.json) {
      callback(null, alert.message(alert.CONST.INFO_JSON_RETURNED, user));
    } else {
      callback(null, user);
    }
  });
};

UserSchema.statics.updateUserRemainingPoint = function(options, callback) {
  this
  .where({_id: options.userId})
  .update({$inc: {remainingPoint: options.data}}, function(err, result) {
    console.log("user", result);
    callback(err, result);
  });
};

var UserModel = mongoose.model('User', UserSchema);

module.exports = UserModel;