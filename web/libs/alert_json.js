var alert_status_string = ["", "ok", "info", "warn", "error"];

var AlertJSON = {
  language: "en",
  
  CONST: require('./constants'),
  
  message: function(code, extra) {
    var status = alert_status_string[Math.floor(parseInt(code) / 1000)];
    if (extra) {
      return {status: status, code: code, message: this.getMessageTable()[code], data: extra};
    } else {
      return {status: status, code: code, message: this.getMessageTable()[code]};
    }
  },
  
  getMessageTable: function() {
    return require('./lang/' + this.language);
  }
}

module.exports = AlertJSON;