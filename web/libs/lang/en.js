module.exports = {
  // ========= normal 1xxx ==========
  "1000" :  "User logged in.",
  "1001" :  "User logged out.",
  "1002" :  "json",
  "1003" :  "html",
  "1004" :  "Data updated.",
  "1005" :  "Data not updated.",
  // ========= info 2xxx ==========
  // ========= warn 3xxx ==========
  // ========= errors 4xxx ===========
  "4000" :  "Something went wrong on server. Please call admin.",
  "4001" :  "No login information.",
  "4002" :  "Invalid request path. No permission.",
  "4003" :  "Invalid request path. No resource found.",
  "4004" :  "An ID - email or FacebookID - is required.",
  "4005" :  "Password is required.",
  "4006" :  "Error while checking user.",
  "4007" :  "Tried to login with your Facebook. However, your account has not been approved. Please contact admin.",
  "4008" :  "This user has not been registered. Please contact admin.",
  "4009" :  "Invalid FacebookID. Please contact an Admin to fix it.",
  "4010" :  "Wrong password.",
  "4011" :  "Database action error.",
  "4012" :  "No user found.",
  "4013" :  "No match found.",
  "4014" :  "Your selected match was updated or removed. Please refresh and play with a newer list of matches.",
  "4015" :  "Your selected market was updated or removed. Please refresh and play with a newer list of markets.",
  "4016" :  "Your selected odd was updated or removed. Please refresh and play with a newer list of odds.",
  "4017" :  "Slip already added. Please inform admin to fix the problem.",
  "4018" :  "Slip already removed. Please inform admin to fix the problem.",
  "4019" :  "Sorry. The betting for the match has been closed. Please select a later match.",
};