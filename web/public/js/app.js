//var DATETIME_FORMAT = "DD/MM/YYYY HH:mm Z";
var DATETIME_FORMAT = "DD/MM/YYYY HH:mm";
var collapsedMatchItemHeight = 28;
var collapsedMarketItemHeight = 21;

$(window).on('resize orientationChange', function() {
  //$('#left-panel').css('left', 0).addClass('hidden-xs');
  //$('#right-panel').css('right', 0).addClass('hidden-xs');
  console.log("vvv");
  $('.modal-backdrop.in').each(function() {
    $(this).remove();
  })
});

function showModalDialog(dialog) {
  // hide all previos modal dialog first
  $('.modal').each(function (){
    $(this).modal('hide');
  });

  $('#popup-dialog').modal({
    keyboard: dialog.keyboard || true,
    show: true
  });

  if (dialog.title) {
    $('#popup-dialog-title').html(dialog.title);
  }

  var color = null;
  if (dialog.type == 'danger') {
    color = '#ebccd1';
  } else if (dialog.type == 'warning') {
    color = '#faebcc';
  } else if (dialog.type == 'info') {
    color = '#ccebd1';
  }
  if (color) {
    $('#popup-dialog .modal-header').css('background-color', color);
    $('#popup-dialog .modal-content').css('border', '2px solid ' + color);
  }

  if (dialog.content) {
    $('#popup-dialog-content').html(dialog.content);
  }

  if (dialog.btn1) {
    if (dialog.btn1.text) {
      $('#popup-dialog-btn1').html(dialog.btn1.text);
    }
    $('#popup-dialog-btn1').unbind('click').bind('click', function() {
      if (dialog.btn1.onclick) {
        dialog.btn1.onclick();
      }
      $('#popup-dialog').modal('hide');
    });
  }

  if (dialog.btn2) {
    $('#popup-dialog-btn2').removeClass('hidden');

    if (dialog.btn2.text) {
      $('#popup-dialog-btn2').html(dialog.btn2.text);
    }

    $('#popup-dialog-btn2').unbind('click').bind('click', function() {
      if (dialog.btn2.onclick) {
        dialog.btn2.onclick();
      }
      $('#popup-dialog').modal('hide');
    });
  } else {
    $('#popup-dialog-btn2').addClass('hidden');
  }
}

function dialogLoginAgain() {
  showModalDialog({
    type: "danger",
    title: "Please login again",
    content: "Your session has expired.",
    btn1: {
      text: "OK",
      onclick: function() {
        clearPageComponent();
      }
    }
  });
}

function showError(json) {
  showModalDialog({
    type: "danger",
    title: "Error " + json.code,
    content: json.message,
    btn1: {text: "OK"}
  });
}

function requestJSON(options) {
  $.ajax({
    url: options.url,
    method: options.method,
    dataType: "json",
    data: options.data,
    cache: options.cache,
    success: function(json) {
      if (json.code == "4001") {
        // session expired, prompt for relogin
        dialogLoginAgain();
        return;
      }
      options.success(json);
    },
    error: function(xhr, status, err) {
      if (options.error) options.error(xhr, status, err);
      if (xhr.responseJSON && xhr.status && xhr.statusText) {
        console.error({status: status, text: xhr.statusText, json: xhr.responseJSON});
      } else {
        console.error(xhr, status, err);
      }
    }
  });
}

// sequentially fetch list of scripts
function fetchScripts(list, allScripts) {
  if (!allScripts) {
    allScripts = "";
  }
  // no more file to get
  if (list.length === 0) {
    // execute them
    try {
      if (JSXTransformer && JSXTransformer.exec) JSXTransformer.exec(allScripts, {});
      else eval(allScripts);
    } catch(e) {
      if (e instanceof ReferenceError) {
        eval(allScripts);
      } else {
        throw e;
      }
    }
    return;
  }

  file = list.shift();
  $.ajax({
    url: file,
    dataType: "text",
    success: function(sd) {
      var lineStart = allScripts.split(/\r\n|\r|\n/).length;
      //console.log(file, lineStart);
      allScripts += "\n" + sd;
      fetchScripts(list, allScripts);
    },
    error: function(s, t, j) {
      console.error(s, t, j);
    }
  });
}

function roundFloat2dp(val) {
  return Math.round((val + 0.00001) * 100) / 100;
}