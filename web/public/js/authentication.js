
function logIntoApp(data) {
  stopFBDetection();
  if (data.FB !== undefined) {
    FB.api('/me', function(response) {
      if (response.error != null) {
        console.error(response.error);
        logOut(data);
        return;
      }
      
      requestJSON({
        url: 'login',
        method: 'POST',
        data: {id: response.id, email: response.email, name: response.name},
        success: function(json) {
          if (json.status == "ok") {
            onLoggedIn(json);
          } else {
            showError(json);
            logOut(data);
          }
        }
      });
    });
  } else if (data.email && data.password){
    requestJSON({
      url: 'login',
      method: 'POST',
      data: {email: data.email, password: data.password},
      success: function(json) {
        if (json.status == "ok") {
          onLoggedIn(json);
        } else {
          showError(json);
          clearPageComponent();
        }
      }
    });
  }
}

function onLoggedIn(json, count) {
  if (count == 100) {
    console.error("There is a problem loading pageComponent.");
    return;
  }

  try {
      
    if (!pageComponent) {
      if (!count) count = 1;
      setTimeout(onLoggedIn.bind(null, json, ++count), 100);
      return;
    }
  } catch (exception) {
    if (!count) count = 1;
    setTimeout(onLoggedIn.bind(null, json, ++count), 100);
    return;
  }
  
  pageComponent.setState({loggedin: true, data: json.data});

  // load panels
  var list = ["player/menu_panel.js"];
  // admin does not need slip panel but others do
  if (!json.data.isAdmin) {
    list = list.concat([
      "player/animation_item.js",
      "player/slip_item.js",
      "player/user_point.js",
      "player/slip_panel.js"
    ]);
  }
  fetchScripts(list);

  // load menu items
  requestJSON({
    url: "player/menu",
    method: "POST",
    success: function(menuitems) {
      pageComponent.setState({menuitems: menuitems});
    }
  });
}

function logOut(options) {
  stopFBDetection();
  var dataToSend = (options) ? {FB: true} : {};
  requestJSON({
    url: 'logout',
    method: 'POST',
    data: dataToSend,
    success: function(data) {
      // may not need to logout FB if previously logged in with normal username
      if (data.code === "1001") {
        // successfully logged out
        clearPageComponent();
      } else if (data.code === "1000") {
        onLoggedIn(data);
      }
    }.bind(this)
  });
}

function clearPageComponent() {
  pageComponent.setState({
    // logic
    loggedin: false, 
    // components
    error: null, 
    pageContent: null, 
    menuPanel: null, 
    slipPanel: null,
    // data
    title: "HC Soccer Betting Game",
    markets: null,
    slips: null,
    userList: null,
    sponsorList: null,
    userPoint: 0
  });
}


var timerFBDetection = -1;
function detectFBConnection(count) {
  // wait for about 2 seconds
  // if FB Login works it should stop the timer
  if (count == 20) {
    logIntoApp({FB: true});
    return;
  }

  timerFBDetection = setTimeout(detectFBConnection, 100, ++count);
}

function stopFBDetection() {
  if (timerFBDetection == -1) return;
  clearTimeout(timerFBDetection);
  timerFBDetection = -1;
}

$(function() {
  // there might be case where FB cannot be reached
  detectFBConnection(1);
});